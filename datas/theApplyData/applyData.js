/**
 * Created by Administrator on 2015/4/23.
 */
window.applyData = {
  INCOME: [
    {name: "1～50万", code: "1"},
    {name: "50～200万", code: "2"},
    {name: "200～600万", code: "3"},
    {name: "600～2000万", code: "4"},
    {name: "2000万以上", code: "5"}
  ],
  STAFF: [
    {name: "1～50人", code: "1"},
    {name: "51～200人", code: "2"},
    {name: "201～600人", code: "3"},
    {name: "601～2000人", code: "4"},
    {name: "2001人以上", code: "5"}
  ],

  AREA: [
    {name: "50平米以下", code: "1"},
    {name: "50～500平米", code: "2"},
    {name: "500平米以上", code: "3"}
  ]
}
