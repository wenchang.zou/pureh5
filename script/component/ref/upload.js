/**
 * Created by wenchang on 2015/5/17.
 */
(function($, M, W){
    M.uploadPreview=function(opts){
        function UploadPreview(opts){
            this.opts=opts || {};
            this.width=this.opts.width;
            this.height=this.opts.height;
            this.imgsDiv=this.opts.imgsDiv || ".imgdiv";
            this.imgUrl=this.opts.imgUrl;
            this.imgType=this.opts.imgType || ["gif", "jpeg", "jpg", "bmp", "png"];
            this.callback=this.opts.callback || function(){return false};
            this.fileCon=this.opts.fileCon;
            this.maxwidth=this.opts.maxwidth || 1900;
            this.maxheight=this.opts.maxheight || 2800;

            return this;

        }
        UploadPreview.prototype={
            init:function(){
                var self=this;
                $(self.imgsDiv).width(self.width);
                $(self.imgsDiv).height(self.height);
                this.changeView();
            },

            autoScaling:function(){

                var imgDiv=$(this.imgsDiv), self=this,_this=this.fileCon, img_width, img_height;


                img_width = this.width;
                img_height = this.height;
                if ($.browser.version == "7.0" || $.browser.version == "8.0")
                {
                    imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";
                }

                if (img_width > self.maxwidth || img_height > self.maxheight) {

                    SY.alertBox({
                        type:"mini",
                        msg:"图片大小不符合要求"
                    });
                    clearvalue(_this[0]);
                    _this.trigger("blur");

                    imgDiv.hide();
                    return false;
                }
                if (img_width > 0 && img_height > 0) {
                    var rate = (self.width / img_width < self.height / img_height) ? self.width / img_width : self.height / img_height;
                    if (rate <= 1) {
                        if ($.browser.version == "7.0" || $.browser.version == "8.0")
                        {
                            imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "scale";
                        }
                        imgDiv.width(img_width * rate);
                        imgDiv.height(img_height * rate);
                    } else {
                        imgDiv.width(img_width);
                        imgDiv.height(img_height);
                    }
                    var left = (self.width - imgDiv.width()) * 0.5;
                    var top = (self.height - imgDiv.height()) * 0.5;
                    imgDiv.css({ "margin-left": left, "margin-top": top });
                    imgDiv.show();
                }
                if ($.browser.version == "7.0" || $.browser.version == "8.0") {
                    imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";
                }

            },
            changeView:function(){
                var self=this, imgDiv=$(this.imgsDiv), _this=this.fileCon;
                this.fileCon.on("change",function(){
                    if (this.value) {
                        if (!RegExp("\.(" + self.imgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
                            SY.alertBox({
                                type:"mini",
                                msg:"图片类型必须是" + self.imgType.join("，") + "中的一种"
                            });

                            clearvalue(_this[0]);
                            return false;
                        }
                        imgDiv.hide();
                        if ($.browser.msie) {
                            if ($.browser.version == "6.0") {
                                var img = $("<img />");
                                imgDiv.replaceWith(img);
                                imgDiv = img;

                                var image = new Image();
                                image.src = 'file:///' + this.value;

                                imgDiv.attr({'src': image.src,"width":self.width,"height":self.height});
                                self.autoScaling();
                            }
                            else {
                                imgDiv.css({ filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image)" });
                                imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";

                                try {
                                    imgDiv.get(0).filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src =(_this[0].value);
                                } catch (e) {
                                    alert("无效的图片文件！");
                                    clearvalue(_this[0]);
                                    return false;
                                }
                                setTimeout(self.autoScaling(), 100);

                            }
                        }
                        else {
                            var img = $("<img />");
                            imgDiv.replaceWith(img);
                            imgDiv = img;
                            //imgDiv.attr('src', this.files.item(0).getAsDataURL());
                            imgDiv.attr('src',window.URL.createObjectURL(this.files.item(0)));
                            imgDiv.css({ "vertical-align": "middle","width":self.width+"px","height":self.height+"px"});
                            setTimeout(self.autoScaling(), 100);
                        }
                    }

                });
            }
        }
        return new UploadPreview(opts).init();
    }
})(Zepto,window.SY=window.SY || {}, window);

//清空上传控件的值，obj为上传控件对象
function clearvalue(obj) {
    obj.select();
    document.execCommand("delete");
}