/*
 * @author cherry
 * Date: 14-7-8
 */
(function(w, d, M){
    var htmlToDom = function(arg){
    var objE = document.createElement("div");
        objE.innerHTML = arg;
       return objE.childNodes;
    };

    M.alertBox = function(opts){

        function AlertBox(opts){

            this.opts = opts || {};
            this.title = this.opts.title || '';
            this.msg = this.opts.msg || '';
            this.confirmMsg = this.opts.confirmMsg || '确定';
            this.cancelMsg = this.opts.cancelMsg || '取消';
            this.type = this.opts.type || 'default';
            this.hasMask = (this.opts.hasMask === false) ? false : true;
            this.uid = this.opts.uid || "#alertBox";
            this.confirm = this.opts.confirm || function(){};
            this.cancel = this.opts.cancel || function(){};
            this.callBack = this.opts.callBack || function(){};

        }
        AlertBox.prototype = {
            init: function(){
                var self = this;
                switch(self.type) {
                    case "default":
                        var html = '<div id="alertBox" class="alertBox"><div class="msg">${msg}</div><div class="layout wbox">' +
                            '<div class="btn-cancel sn-btn-b wbox-flex block mr10"><a href="javascript:void(0)"><span>'+this.cancelMsg+'</span></a></div>' +
                            '<div class="btn-confirm sn-btn-c wbox-flex block"><a href="javascript:void(0)"><span>'+this.confirmMsg+'</span></a></div></div></div>';
                        break;
                    case "alert":
                        var html = '<div id="alertBox" class="alertBox"><div class="msg">${msg}</div>' +
                            '<div class="layout wbox"><div class="btn-confirm sn-btn-c wbox-flex block"><a href="javascript:void(0)">'+this.confirmMsg+'</a></div></div></div>';
                        break;
                    case "mini":
                        var html = '<div id="alertBox" class="alertBox alertBoxBlack"><div class="msg">${msg}</div></div>';
                        break;
                    case 'inner':
                        var html = '<div id="alertBox" class="alertBox alertBox-inner">${msg}</div>';
                }
                var tpl = this.HTMLTemplate({
                    string: html,
                    para: {
                        msg: self.msg
                    }
                });
                self.render(tpl);
            },
            render: function(_htmlTpl){
                var self = this;
                var body = document.querySelector("body");
                !document.querySelector(self.uid) && body.insertAdjacentHTML('beforeend', _htmlTpl);
                (typeof self.callBack == "function") && self.callBack();
                self.hasMask && self.mask(body);
                self.setPos(document.querySelector(self.uid));
            },
            mask: function(body){
                var maskEl = document.createElement("div");
                maskEl.id = 'tempMask';
                var _height = this.getHeight();
                body.appendChild(maskEl);
                document.querySelector("#tempMask").style.cssText = ";height:" + _height + "px;width:" + document.documentElement.offsetWidth + "px;";
            },
            HTMLTemplate:function(opts){
                var o = opts.para || {};
                var html = [opts.string].join('');
                if (o) {
                    for (var i in o) {
                        //替换掉${msg}
                        html = html.replace(/\$\{(\w+)\}/g, o[i]); // "\"转义符,先匹配
                    }
                    return html;
                }
                return html;
            },
            setPos: function(El){
                var self = this;
                var scrollTop = this.scrollTop();
                El.style.cssText = ";top:" + (scrollTop + window.innerHeight/2 - El.offsetHeight/2) + "px;left:" + (document.documentElement.offsetWidth/2 - El.offsetWidth/2) + "px;display:block;";
                if(self.type == "mini"){
                    document.querySelector("#tempMask").style.opacity = 0;

                    setTimeout(function(){

                        $("#tempMask").remove();
                        $("#alertBox").remove();

                    },2000);
                    return;
                }
                self.addEvent(El);
            },
            addEvent: function(El){
                var self = this;
                $(El.querySelector(".btn-confirm")).on("click", function(e){
                    self.confirm(El);
                    self.reset(El);
                    e.preventDefault();
                });
                if(self.type != "alert"){
                    $(El.querySelector(".btn-cancel")).on("click", function(e){
                        self.cancel(El);
                        self.reset(El);
                        e.preventDefault();
                    });
                }
            },
            reset: function(El){
                if(document.querySelector("#tempMask")){
                    this.remove.call(document.querySelector("#tempMask"));
                }
                this.remove.call(El);
                if(this.type != "mini"){
                    this.die(El);
                }
            },
            die: function(El){
                $(El.querySelector(".btn-confirm")).off("click");
                if(self.type != "alert"){
                    $(El.querySelector(".btn-cancel")).off("click");
                }
            },
            getHeight: function(){
                var height = document.documentElement.offsetHeight || document.body.offsetHeight;
                if(window.innerHeight > height){
                    height = window.innerHeight;
                }
                return height;
            },
            scrollTop: function(){
                return document.documentElement.scrollTop || document.body.scrollTop;
            },
            remove: function(){
                return this.parentNode.removeChild(this);
            }


        };
        new AlertBox(opts).init();
    }
    M.bannerImage=function(opts){
        function BannerImage(opts){
            this.opts=opts || {};
            this.myImg=this.opts.myImg || $(".myImg");
            this.logoH=this.opts.logoH || $(".logo-layer-con");
            this.listLen=this.opts.listLen;
            this.slideUl=this.opts.slideUl;
            this.parentUl=this.opts.parentUl || $(".index-slide");
            this.middleCon=this.opts.middleCon || $(".top-middle-con");
        }
        BannerImage.prototype={
            init:function(){
                this.bannerSize();
            },
            bannerSize:function(){
                var len, widths,imgH,logoH,that=this,sizes;
                window.addEventListener("resize",function(){
                    size();
                });

                var size=function(){

                    len=that.listLen.size();
                    sizes=that.parentUl.children("ul").children("li");
                    widths=$(".main-wrap").width() >=640 ? 640 : $(".main-wrap").width();
                    imgH=that.myImg.height();
                    that.slideUl.width(widths*len+"px");
                    that.parentUl.find("img").css({height:($(".main-wrap").width()/640)*220+"px"});

                    that.parentUl.css({height:($(".main-wrap").width()/640)*220+"px"});
                    that.middleCon.height(($(".main-wrap").width()/640)*354);
                    logoH=that.logoH.height(that.middleCon.height()-($(".main-wrap").width()/640)*220);
                    for(var i=0; i<len; i++){
                        that.listLen[i].style.width=widths+"px";
                        that.listLen[i].style.left=widths*i+"px";
                    }

                };
                size();

            }
        }
        return new BannerImage(opts).init();
    }
})(window, window.document, window.SY = window.SY || {});
