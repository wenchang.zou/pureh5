/**
 * Created by wenchang on 2015/5/17.
 */
;(function($, M, W){
    M.uploadPreview=function(opts){
        function UploadPreview(opts){
            this.opts=opts || {};
            this.width=this.opts.width;
            this.height=this.opts.height;
            this.imgsDiv=this.opts.imgsDiv || $(".img-icon");
            this.imgUrl=this.opts.imgUrl;
            this.imgType=this.opts.imgType || ["gif", "jpeg", "jpg", "bmp", "png"];
            this.callback=this.opts.callback || function(){return false};
            this.fileCon=this.opts.fileCon;
            this.maxwidth=this.opts.maxwidth || 1900;
            this.maxheight=this.opts.maxheight || 2800;
            this.divTxt=this.opts.divTxt;
            this.form=this.opts.form;
            return this;

        }
        UploadPreview.prototype={
            init:function(){
                var self=this;
                self.imgsDiv.width(self.width+"rem");
                self.imgsDiv.height(self.height+"rem");
                this.changeView();
            },
            upLoads:function(dom){
              var self=this,formData= new FormData(self.form[0]),len,items,src,arr=[];
              M.Req.uploadImg(function(data){
                len=data.result.length;
                for(var i=0; i<len; i++){
                  items=data.result[i];
                  arr.push(M.Const.PRO_PATH+items.path);
                }
                dom.attr("src",arr.join(''));
                dom.attr("data-event-name","img-btn");
                self.divTxt.text(arr.join(''));
              },formData);
            },
            autoScaling:function(){

                var imgDiv=this.imgsDiv, self=this,_this=this.fileCon, img_width, img_height;


                img_width = imgDiv.width();
                img_height = imgDiv.height();
                if ($.browser.version == "7.0" || $.browser.version == "8.0")
                {
                    imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";
                }

                if (img_width > self.maxwidth || img_height > self.maxheight) {
                    SY.alertBox({
                       type:"mini",
                       msg:"图片大小不符合要求"
                    });
                    clearvalue(_this[0]);
                    _this.trigger("blur");

                    imgDiv.hide();
                    return false;
                }
                if (img_width > 0 && img_height > 0) {
                    var rate = (self.width / img_width < self.height / img_height) ? self.width / img_width : self.height / img_height;
                    if (rate <= 1) {
                        if ($.browser.version == "7.0" || $.browser.version == "8.0")
                        {
                            imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "scale";
                        }
                        imgDiv.width(img_width * rate);
                        imgDiv.height(img_height * rate);
                    } else {
                        imgDiv.width(img_width);
                        imgDiv.height(img_height);
                    }
                    var left = (self.width - imgDiv.width()) * 0.5;
                    var top = (self.height - imgDiv.height()) * 0.5;
                    imgDiv.css({ "margin-left": left, "margin-top": top });
                    imgDiv.show();
                }
                if ($.browser.version == "7.0" || $.browser.version == "8.0") {
                    imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";
                }

            },
            changeView:function(){
                var self=this, imgDiv=$(this.imgsDiv), _this=this.fileCon;
                this.fileCon.on("change",function(){

                    if (this.value) {
                        if (!RegExp("\.(" + self.imgType.join("|") + ")$", "i").test(this.value.toLowerCase())) {
                          WP.alertBox({
                            type:"mini",
                            msg:"图片类型必须是" + self.imgType.join("，") + "中的一种"
                          });

                            clearvalue(_this[0]);
                            return false;
                        }
                        imgDiv.hide();
                        if ($.browser.msie) {
                            if ($.browser.version == "6.0") {
                                var img = $("<img />");
                                imgDiv.replaceWith(img);
                                imgDiv = img;

                                var image = new Image();


                                imgDiv.attr({'src': self.upLoads(imgDiv),"width":self.width+"rem","height":self.height+"rem"});
                                self.upLoads(imgDiv);

                                self.autoScaling();
                            }
                            else {
                                imgDiv.css({ filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image)" });
                                imgDiv.get(0).filters.item("DXImageTransform.Microsoft.AlphaImageLoader").sizingMethod = "image";

                                try {
                                     self.upLoads(imgDiv);
                                    //imgDiv.get(0).filters.item('DXImageTransform.Microsoft.AlphaImageLoader').src =(self.upLoad());
                                } catch (e) {
                                    WP.alertBox({
                                      type:"mini",
                                      msg:"无效的图片格式"
                                    });
                                    clearvalue(_this[0]);
                                    return false;
                                }
                                setTimeout(self.autoScaling(), 100);

                            }
                        }
                        else {
                            var img = $("<img />");
                            imgDiv.replaceWith(img);
                            imgDiv = img;
                            //imgDiv.attr('src', this.files.item(0).getAsDataURL());

                            imgDiv.css({ "vertical-align": "middle","width":self.width+"rem","height":self.height+"rem"});
                            self.upLoads(imgDiv);

                            setTimeout(self.autoScaling(), 100);
                        }
                    }

                });
            }
        }
        return new UploadPreview(opts).init();
    }
})(Zepto,window.SY=window.SY || {}, window);

//清空上传控件的值，obj为上传控件对象
function clearvalue(obj) {
    obj.select();
    document.execCommand("delete");
}
