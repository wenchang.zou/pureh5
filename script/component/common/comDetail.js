$(function(){
    //分享
    $("#share").on("click",function(){
        $(".am-share").addClass("am-modal-active"); 
        if($(".sharebg").length>0){
            $(".sharebg").addClass("sharebg-active");
        }else{
            $("body").append('<div class="sharebg"></div>');
            $(".sharebg").addClass("sharebg-active");
        }
        $(".sharebg-active,.share-btn").click(function(){
            $(".am-share").removeClass("am-modal-active");  
            setTimeout(function(){
                $(".sharebg-active").removeClass("sharebg-active"); 
                $(".sharebg").remove(); 
            },300);
        })
    })


    //租赁详情选项卡
    $("#zlgs-detail-tab li").on("click",function(){
        var index=$(this).index();
        $("#zlgs-detail-tab li").eq(index).addClass("current").siblings().removeClass("current");
        $(".zlgs-item").eq(index).addClass("on").siblings().removeClass("on");
    })

})