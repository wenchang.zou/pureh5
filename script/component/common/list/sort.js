$(function(){
        var Initials=$('.initials'),
            Initials2=$('.initials2'),
            Initials3=$('.initials3'),
            Initials4=$('.initials4');
        var LetterBox=$('#letter'),
            LetterBox2=$('#letter2'),
            LetterBox3=$('#letter3'),
            LetterBox4=$('#letter4');

        var windowHeight=$(window).height(),
            windowHeight2=$(window).height(),
            windowHeight3=$(window).height(),
            windowHeight4=$(window).height();
        var InitHeight=windowHeight-45,
            InitHeight2=windowHeight2-45,
            InitHeight3=windowHeight3-45,
            InitHeight4=windowHeight4-45;
        Initials.height(InitHeight);
        Initials2.height(InitHeight2);
        Initials3.height(InitHeight3);
        Initials4.height(InitHeight4);

        var LiHeight=InitHeight/26,
            LiHeight2=InitHeight2/26,
            LiHeight3=InitHeight3/26,
            LiHeight4=InitHeight4/26;
        Initials.find('li').height(LiHeight);
        Initials2.find('li').height(LiHeight2);
        Initials3.find('li').height(LiHeight3);
        Initials4.find('li').height(LiHeight4);
        
        //所在地
        Initials.find('ul').append('<li>A</li><li>B</li><li>C</li><li>F</li><li>H</li><li>J</li><li>L</li><li>N</li><li>Q</li><li>S</li><li>T</li><li>X</li><li>Y</li><li>Z</li>');
        initials();
        $(".initials").children("ul").children("li").each(function(i){
             var LetterHtml=$(this).html();
                $(this).wrapInner("<a href=#"+LetterHtml+"></a>");
                
            $(this).click(function(){
                var _this=$(this);
                LetterBox.html(LetterHtml).fadeIn();
                Initials.css('background','rgba(145,145,145,0.6)');
                setTimeout(function(){
                    Initials.css('background','rgba(145,145,145,0)');
                    LetterBox.fadeOut();
                },1000);

                var _index = _this.index()
                if(_index==0){
                    $('#scroller').animate({scrollTop: '0px'}, 300);//点击第一个滚到顶部
                }else if(_index==27){
                    var DefaultTop=$('#default').offset().top;
                    $('#scroller').animate({scrollTop: DefaultTop+'px'}, 300);//点击最后一个滚到#号
                }else{
                    var letter = _this.text();
                    if($('#'+letter).length>0){
                        var LetterTop = $('#'+letter).offset().top;
                    }
                }
            })
        });
       
        //设备类型
        Initials2.find('ul').append('<li>H</li><li>L</li><li>Q</li><li>S</li><li>T</li><li>Z</li>');
        initials2();
        $(".initials2").children("ul").children("li").each(function(i){
            var LetterHtml2=$(this).html();
            $(this).wrapInner("<a href=#"+LetterHtml2+"1></a>");
        
            $(this).click(function(){
                var _this2=$(this);
                LetterBox2.html(LetterHtml2).fadeIn();
                Initials2.css('background','rgba(145,145,145,0.6)');
                setTimeout(function(){
                    Initials2.css('background','rgba(145,145,145,0)');
                    LetterBox2.fadeOut();
                },1000);

                var _index2 = _this2.index()
                if(_index2==0){
                    $('#scroller2').animate({scrollTop: '0px'}, 300);//点击第一个滚到顶部
                }else if(_index2==27){
                    var DefaultTop=$('#default2').offset().top;
                    $('#scroller2').animate({scrollTop: DefaultTop+'px'}, 300);//点击最后一个滚到#号
                }else{
                    var letter2 = _this2.text();
                    if($('#'+letter2).length>0){
                        var LetterTop = $('#'+letter2).offset().top;
                        //$('html,body').animate({scrollTop: LetterTop-45+'px'}, 300);
                    }
                }
            })
        });

        //品牌型号
        Initials3.find('ul').append('<li>D</li><li>K</li><li>S</li><li>W</li><li>X</li><li>Z</li>');
        initials3();
        $(".initials3").children("ul").children("li").each(function(i){
                var LetterHtml3=$(this).html();
                $(this).wrapInner("<a href=#"+LetterHtml3+"2></a>");
            $(this).click(function(){
                var _this3=$(this);
                LetterBox3.html(LetterHtml3).fadeIn();

                Initials3.css('background','rgba(145,145,145,0.6)');
                setTimeout(function(){
                    Initials3.css('background','rgba(145,145,145,0)');
                    LetterBox3.fadeOut();
                },1000);

                var _index3 = _this3.index()
                if(_index3==0){
                    $('#scroller3').animate({scrollTop: '0px'}, 300);//点击第一个滚到顶部
                }else if(_index3==27){
                    var DefaultTop=$('#default3').offset().top;
                    $('#scroller3').animate({scrollTop: DefaultTop+'px'}, 300);//点击最后一个滚到#号
                }else{
                    var letter3 = _this3.text();
                    if($('#'+letter3).length>0){
                        var LetterTop = $('#'+letter3).offset().top;
                        //$('#scroller3').animate({scrollTop: LetterTop-45+'px'}, 300);
                    }
                }
            })
        });

        //维修机型
        Initials4.find('ul').append('<li>C</li><li>G</li><li>Q</li><li>W</li><li>Y</li><li>Z</li>');
        initials4();
        $(".initials4").children("ul").children("li").each(function(i){
                var LetterHtml4=$(this).html();
                $(this).wrapInner("<a href=#"+LetterHtml4+"3></a>");
            $(this).click(function(){
                var _this4=$(this);
                LetterBox4.html(LetterHtml4).fadeIn();

                Initials4.css('background','rgba(145,145,145,0.6)');
                setTimeout(function(){
                    Initials4.css('background','rgba(145,145,145,0)');
                    LetterBox4.fadeOut();
                },1000);

                var _index4 = _this4.index()
                if(_index4==0){
                    $('#scroller4').animate({scrollTop: '0px'}, 300);//点击第一个滚到顶部
                }else if(_index4==27){
                    var DefaultTop=$('#default4').offset().top;
                    $('#scroller4').animate({scrollTop: DefaultTop+'px'}, 300);//点击最后一个滚到#号
                }else{
                    var letter4 = _this4.text();
                    if($('#'+letter4).length>0){
                        var LetterTop = $('#'+letter4).offset().top;
                        //$('#scroller3').animate({scrollTop: LetterTop-45+'px'}, 300);
                    }
                }
            })
        });

        //工作年限
        /*Initials5.find('ul').append('<li>A</li>');
        initials5();
        $(".initials5").children("ul").children("li").each(function(i){
                var LetterHtml5=$(this).html();
                $(this).wrapInner("<a href=#"+LetterHtml5+"4></a>");
            $(this).click(function(){
                var _this5=$(this);
                LetterBox5.html(LetterHtml5).fadeIn();

                Initials5.css('background','rgba(145,145,145,0.6)');
                setTimeout(function(){
                    Initials5.css('background','rgba(145,145,145,0)');
                    LetterBox5.fadeOut();
                },1000);

                var _index5 = _this5.index()
                if(_index5==0){
                    $('#scroller5').animate({scrollTop: '0px'}, 300);//点击第一个滚到顶部
                }else if(_index5==27){
                    var DefaultTop=$('#default5').offset().top;
                    $('#scroller5').animate({scrollTop: DefaultTop+'px'}, 300);//点击最后一个滚到#号
                }else{
                    var letter5 = _this5.text();
                    if($('#'+letter5).length>0){
                        var LetterTop = $('#'+letter5).offset().top;
                        //$('#scroller3').animate({scrollTop: LetterTop-45+'px'}, 300);
                    }
                }
            })
        });*/

})

//所在地
function initials() {//公众号排序
    var SortList=$(".sort-list"),
        SortBox=$(".sort-box"),
        initials = [],
        num=0;

    SortList.sort(asc_sort).appendTo('.sort-box');//按首字母排序
    function asc_sort(a, b) {
        return makePy($(b).find('.num-name').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.num-name').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    SortList.each(function(i) {
        var initial = makePy($(this).find('.num-name').text().charAt(0))[0].toUpperCase();
        if(initial>='A'&&initial<='Z'){
            if (initials.indexOf(initial) === -1)
                initials.push(initial);
        }else{
            num++;
        }
        
    });

    $.each(initials, function(index, value) {//添加首字母标签
        SortBox.append('<div class="sort-letter" id="'+ value +'">' + value + '</div>');
    });
    //if(num!=0){SortBox.append('<div class="sort-letter" id="default">#</div>');}

    for (var i =0;i<SortList.length;i++) {//插入到对应的首字母后面
        var letter=makePy(SortList.eq(i).find('.num-name').text().charAt(0))[0].toUpperCase();
        switch(letter){
            case "A":
                $('#A').after(SortList.eq(i));
                break;
            case "B":
                $('#B').after(SortList.eq(i));
                break;
            case "C":
                $('#C').after(SortList.eq(i));
                break;
            case "D":
                $('#D').after(SortList.eq(i));
                break;
            case "E":
                $('#E').after(SortList.eq(i));
                break;
            case "F":
                $('#F').after(SortList.eq(i));
                break;
            case "G":
                $('#G').after(SortList.eq(i));
                break;
            case "H":
                $('#H').after(SortList.eq(i));
                break;
            case "I":
                $('#I').after(SortList.eq(i));
                break;
            case "J":
                $('#J').after(SortList.eq(i));
                break;
            case "K":
                $('#K').after(SortList.eq(i));
                break;
            case "L":
                $('#L').after(SortList.eq(i));
                break;
            case "M":
                $('#M').after(SortList.eq(i));
                break;
            case "N":
                $('#N').after(SortList.eq(i));
                break;
            case "O":
                $('#O').after(SortList.eq(i));
                break;
            case "P":
                $('#P').after(SortList.eq(i));
                break;
            case "Q":
                $('#Q').after(SortList.eq(i));
                break;
            case "R":
                $('#R').after(SortList.eq(i));
                break;
            case "S":
                $('#S').after(SortList.eq(i));
                break;
            case "T":
                $('#T').after(SortList.eq(i));
                break;
            case "U":
                $('#U').after(SortList.eq(i));
                break;
            case "V":
                $('#V').after(SortList.eq(i));
                break;
            case "W":
                $('#W').after(SortList.eq(i));
                break;
            case "X":
                $('#X').after(SortList.eq(i));
                break;
            case "Y":
                $('#Y').after(SortList.eq(i));
                break;
            case "Z":
                $('#Z').after(SortList.eq(i));
                break;
            default:
                $('#default').after(SortList.eq(i));
                break;
        }
    };
}

//设备类型
function initials2() {//公众号排序
    var SortList2=$(".sort-list2"),
        SortBox2=$(".sort-box2"),
        initials2 = [],
        num2=0;

    SortList2.sort(asc_sort2).appendTo('.sort-box2');//按首字母排序
    function asc_sort2(a, b) {
        return makePy($(b).find('.num-name2').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.num-name2').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    SortList2.each(function(i) {
        var initial2 = makePy($(this).find('.num-name2').text().charAt(0))[0].toUpperCase();
        if(initial2>='A'&&initial2<='Z'){
            if (initials2.indexOf(initial2) === -1)
                initials2.push(initial2);
        }else{
            num2++;
        }
        
    });

    $.each(initials2, function(index, value) {//添加首字母标签
        SortBox2.append('<div class="sort-letter2" id="'+ value +'1">' + value + '</div>');
    });
    //if(num!=0){SortBox.append('<div class="sort-letter2" id="default2">#</div>');}

    for (var i =0;i<SortList2.length;i++) {//插入到对应的首字母后面
        var letter2=makePy(SortList2.eq(i).find('.num-name2').text().charAt(0))[0].toUpperCase();
        switch(letter2){
            case "A":
                $('#A1').after(SortList2.eq(i));
                break;
            case "B":
                $('#B1').after(SortList2.eq(i));
                break;
            case "C":
                $('#C1').after(SortList2.eq(i));
                break;
            case "D":
                $('#D1').after(SortList2.eq(i));
                break;
            case "E":
                $('#E1').after(SortList2.eq(i));
                break;
            case "F":
                $('#F1').after(SortList2.eq(i));
                break;
            case "G":
                $('#G1').after(SortList2.eq(i));
                break;
            case "H":
                $('#H1').after(SortList2.eq(i));
                break;
            case "I":
                $('#I1').after(SortList2.eq(i));
                break;
            case "J":
                $('#J1').after(SortList2.eq(i));
                break;
            case "K":
                $('#K1').after(SortList2.eq(i));
                break;
            case "L":
                $('#L1').after(SortList2.eq(i));
                break;
            case "M":
                $('#M1').after(SortList2.eq(i));
                break;
            case "N":
                $('#N1').after(SortList2.eq(i));
                break;
            case "O":
                $('#O1').after(SortList2.eq(i));
                break;
            case "P":
                $('#P1').after(SortList2.eq(i));
                break;
            case "Q":
                $('#Q1').after(SortList2.eq(i));
                break;
            case "R":
                $('#R1').after(SortList2.eq(i));
                break;
            case "S":
                $('#S1').after(SortList2.eq(i));
                break;
            case "T":
                $('#T1').after(SortList2.eq(i));
                break;
            case "U":
                $('#U1').after(SortList2.eq(i));
                break;
            case "V":
                $('#V1').after(SortList2.eq(i));
                break;
            case "W":
                $('#W1').after(SortList2.eq(i));
                break;
            case "X":
                $('#X1').after(SortList2.eq(i));
                break;
            case "Y":
                $('#Y1').after(SortList2.eq(i));
                break;
            case "Z":
                $('#Z1').after(SortList2.eq(i));
                break;
            default:
                $('#default2').after(SortList2.eq(i));
                break;
        }
    };
}

//品牌型号
function initials3() {//公众号排序
    var SortList3=$(".sort-list3"),
        SortBox3=$(".sort-box3"),
        initials3 = [],
        num3=0;

    SortList3.sort(asc_sort3).appendTo('.sort-box3');//按首字母排序
    function asc_sort3(a, b) {
        return makePy($(b).find('.num-name3').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.num-name3').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    SortList3.each(function(i) {
        var initial3 = makePy($(this).find('.num-name3').text().charAt(0))[0].toUpperCase();
        if(initial3>='A'&&initial3<='Z'){
            if (initials3.indexOf(initial3) === -1)
                initials3.push(initial3);
        }else{
            num3++;
        }
        
    });

    $.each(initials3, function(index, value) {//添加首字母标签
        SortBox3.append('<div class="sort-letter3" id="'+ value +'2">' + value + '</div>');
    });
    //if(num3!=0){SortBox3.append('<div class="sort-letter3" id="default3">#</div>');}

    for (var i =0;i<SortList3.length;i++) {//插入到对应的首字母后面
        var letter3=makePy(SortList3.eq(i).find('.num-name3').text().charAt(0))[0].toUpperCase();
        switch(letter3){
            case "A":
                $('#A2').after(SortList3.eq(i));
                break;
            case "B":
                $('#B2').after(SortList3.eq(i));
                break;
            case "C":
                $('#C2').after(SortList3.eq(i));
                break;
            case "D":
                $('#D2').after(SortList3.eq(i));
                break;
            case "E":
                $('#E2').after(SortList3.eq(i));
                break;
            case "F":
                $('#F2').after(SortList3.eq(i));
                break;
            case "G":
                $('#G2').after(SortList3.eq(i));
                break;
            case "H":
                $('#H2').after(SortList3.eq(i));
                break;
            case "I":
                $('#I2').after(SortList3.eq(i));
                break;
            case "J":
                $('#J2').after(SortList3.eq(i));
                break;
            case "K":
                $('#K2').after(SortList3.eq(i));
                break;
            case "L":
                $('#L2').after(SortList3.eq(i));
                break;
            case "M":
                $('#M2').after(SortList3.eq(i));
                break;
            case "N":
                $('#N2').after(SortList3.eq(i));
                break;
            case "O":
                $('#O2').after(SortList3.eq(i));
                break;
            case "P":
                $('#P2').after(SortList3.eq(i));
                break;
            case "Q":
                $('#Q2').after(SortList3.eq(i));
                break;
            case "R":
                $('#R2').after(SortList3.eq(i));
                break;
            case "S":
                $('#S2').after(SortList3.eq(i));
                break;
            case "T":
                $('#T2').after(SortList3.eq(i));
                break;
            case "U":
                $('#U2').after(SortList3.eq(i));
                break;
            case "V":
                $('#V2').after(SortList3.eq(i));
                break;
            case "W":
                $('#W2').after(SortList3.eq(i));
                break;
            case "X":
                $('#X2').after(SortList3.eq(i));
                break;
            case "Y":
                $('#Y2').after(SortList3.eq(i));
                break;
            case "Z":
                $('#Z2').after(SortList3.eq(i));
                break;
            default:
                $('#default3').after(SortList3.eq(i));
                break;
        }
    };
}

//维修机型
function initials4() {//公众号排序
    var SortList4=$(".sort-list4"),
        SortBox4=$(".sort-box4"),
        initials4 = [],
        num4=0;

    SortList4.sort(asc_sort4).appendTo('.sort-box4');//按首字母排序
    function asc_sort4(a, b) {
        return makePy($(b).find('.num-name4').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.num-name4').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    SortList4.each(function(i) {
        var initial4 = makePy($(this).find('.num-name4').text().charAt(0))[0].toUpperCase();
        if(initial4>='A'&&initial4<='Z'){
            if (initials4.indexOf(initial4) === -1)
                initials4.push(initial4);
        }else{
            num4++;
        }
        
    });

    $.each(initials4, function(index, value) {//添加首字母标签
        SortBox4.append('<div class="sort-letter4" id="'+ value +'3">' + value + '</div>');
    });
    //if(num4!=0){SortBox4.append('<div class="sort-letter4" id="default4">#</div>');}

    for (var i =0;i<SortList4.length;i++) {//插入到对应的首字母后面
        var letter4=makePy(SortList4.eq(i).find('.num-name4').text().charAt(0))[0].toUpperCase();
        switch(letter4){
            case "A":
                $('#A3').after(SortList4.eq(i));
                break;
            case "B":
                $('#B3').after(SortList4.eq(i));
                break;
            case "C":
                $('#C3').after(SortList4.eq(i));
                break;
            case "D":
                $('#D3').after(SortList4.eq(i));
                break;
            case "E":
                $('#E3').after(SortList4.eq(i));
                break;
            case "F":
                $('#F3').after(SortList4.eq(i));
                break;
            case "G":
                $('#G3').after(SortList4.eq(i));
                break;
            case "H":
                $('#H3').after(SortList4.eq(i));
                break;
            case "I":
                $('#I3').after(SortList4.eq(i));
                break;
            case "J":
                $('#J3').after(SortList4.eq(i));
                break;
            case "K":
                $('#K3').after(SortList4.eq(i));
                break;
            case "L":
                $('#L3').after(SortList4.eq(i));
                break;
            case "M":
                $('#M3').after(SortList4.eq(i));
                break;
            case "N":
                $('#N3').after(SortList4.eq(i));
                break;
            case "O":
                $('#O3').after(SortList4.eq(i));
                break;
            case "P":
                $('#P3').after(SortList4.eq(i));
                break;
            case "Q":
                $('#Q3').after(SortList4.eq(i));
                break;
            case "R":
                $('#R3').after(SortList4.eq(i));
                break;
            case "S":
                $('#S3').after(SortList4.eq(i));
                break;
            case "T":
                $('#T3').after(SortList4.eq(i));
                break;
            case "U":
                $('#U3').after(SortList4.eq(i));
                break;
            case "V":
                $('#V3').after(SortList4.eq(i));
                break;
            case "W":
                $('#W3').after(SortList4.eq(i));
                break;
            case "X":
                $('#X3').after(SortList4.eq(i));
                break;
            case "Y":
                $('#Y3').after(SortList4.eq(i));
                break;
            case "Z":
                $('#Z3').after(SortList4.eq(i));
                break;
            default:
                $('#default4').after(SortList4.eq(i));
                break;
        }
    };
}

//工作年限
/*function initials5() {//公众号排序
    var SortList5=$(".sort-list5"),
        SortBox5=$(".sort-box5"),
        initials5 = [],
        num5=0;

    SortList5.sort(asc_sort5).appendTo('.sort-box5');//按首字母排序
    function asc_sort5(a, b) {
        return makePy($(b).find('.num-name5').text().charAt(0))[0].toUpperCase() < makePy($(a).find('.num-name5').text().charAt(0))[0].toUpperCase() ? 1 : -1;
    }

    SortList5.each(function(i) {
        var initial5 = makePy($(this).find('.num-name5').text().charAt(0))[0].toUpperCase();
        if(initial5>='A'&&initial5<='Z'){
            if (initials5.indexOf(initial5) === -1)
                initials5.push(initial5);
        }else{
            num5++;
        }
        
    });

    $.each(initials5, function(index, value) {//添加首字母标签
        SortBox5.append('<div class="sort-letter5" id="'+ value +'4"></div>');
    });
    //if(num5!=0){SortBox5.append('<div class="sort-letter5" id="default5">#</div>');}

    for (var i =0;i<SortList5.length;i++) {//插入到对应的首字母后面
        var letter5=makePy(SortList5.eq(i).find('.num-name5').text().charAt(0))[0].toUpperCase();
        switch(letter5){
            case "A":
                $('#A4').after(SortList5.eq(i));
                break;
            case "B":
                $('#B4').after(SortList5.eq(i));
                break;
            case "C":
                $('#C4').after(SortList5.eq(i));
                break;
            case "D":
                $('#D4').after(SortList5.eq(i));
                break;
            case "E":
                $('#E4').after(SortList5.eq(i));
                break;
            case "F":
                $('#F4').after(SortList5.eq(i));
                break;
            case "G":
                $('#G4').after(SortList5.eq(i));
                break;
            case "H":
                $('#H4').after(SortList5.eq(i));
                break;
            case "I":
                $('#I4').after(SortList5.eq(i));
                break;
            case "J":
                $('#J4').after(SortList5.eq(i));
                break;
            case "K":
                $('#K4').after(SortList5.eq(i));
                break;
            case "L":
                $('#L4').after(SortList5.eq(i));
                break;
            case "M":
                $('#M4').after(SortList5.eq(i));
                break;
            case "N":
                $('#N4').after(SortList5.eq(i));
                break;
            case "O":
                $('#O4').after(SortList5.eq(i));
                break;
            case "P":
                $('#P4').after(SortList5.eq(i));
                break;
            case "Q":
                $('#Q4').after(SortList5.eq(i));
                break;
            case "R":
                $('#R4').after(SortList5.eq(i));
                break;
            case "S":
                $('#S4').after(SortList5.eq(i));
                break;
            case "T":
                $('#T4').after(SortList5.eq(i));
                break;
            case "U":
                $('#U4').after(SortList5.eq(i));
                break;
            case "V":
                $('#V4').after(SortList5.eq(i));
                break;
            case "W":
                $('#W4').after(SortList5.eq(i));
                break;
            case "X":
                $('#X4').after(SortList5.eq(i));
                break;
            case "Y":
                $('#Y4').after(SortList5.eq(i));
                break;
            case "Z":
                $('#Z4').after(SortList5.eq(i));
                break;
            default:
                $('#default5').after(SortList5.eq(i));
                break;
        }
    };
}*/