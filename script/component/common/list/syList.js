//列表页通用
(function(W,D,M){
    M.seletedDom=function(opts){
        function SelectedDom(opts){
            this.opts =opts || {};
            this.dom=this.opts.dom;
            this.tDom=this.opts.tDom;
            this.cDom=this.opts.cDom;
            this.init();
            return this;
        }
        SelectedDom.prototype={
            init:function(){
                this.seleted();
            },
            seleted:function(){
                var self=this;
                $(this.dom).each(function(i){

                var _html,
                    me=$(this),
                    $container=$(self.tDom);
                    me.children(self.cDom).on("touchstart click", function(){
                        _html=$(this).html();
                        $container.html(_html+'<span class="filt-arrow cur"></span></li>');
                        $($container).addClass('area-cur');
                    });
                    
            });
            }
        }
        new SelectedDom(opts);
    }
})(window,document,window.SY = window.SY || {});

$(function(){
    //右侧滑动展示
    $('.zlgs-tab ul li,.qz-tab ul li,.sbk-tab ul li,.gcs-tab ul li,.czs-tab ul li').on('click',function(){
        var index=$(this).index();
        event.preventDefault();
        $('#mask').show();
        $('.szd-wrap').eq(index).addClass('is-visible').siblings().removeClass('is-visible');
    });
    $('#mask,.szd-close').on('click',function(event){
        event.preventDefault();
        $('#mask').hide();
        $('.szd-wrap').removeClass('is-visible');
    });  
    
    //点击省市，在父级容器显示其内容
    SY.seletedDom({
        dom:".sort-list",
        tDom:"#list-area",
        cDom:".num-name"

    })
    //设备类型
    SY.seletedDom({
        dom:".sort-list2",
        tDom:"#list-equip",
        cDom:".num-name2"

    })
    //品牌型号
    SY.seletedDom({
        dom:".sort-list3",
        tDom:"#list-brand",
        cDom:".num-name3"

    })
    //维修机型
    SY.seletedDom({
        dom:".sort-list4",
        tDom:"#list-model",
        cDom:".num-name4"

    })
    //工作年限
    SY.seletedDom({
        dom:".sort-list5",
        tDom:"#list-years",
        cDom:".num-name5"

    })
    
})