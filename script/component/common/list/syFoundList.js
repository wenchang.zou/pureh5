//列表页通用
(function(W,D,M){
    M.seletedDom=function(opts){
        function SelectedDom(opts){
            this.opts =opts || {};
            this.dom=this.opts.dom;
            this.tDom=this.opts.tDom;
            this.cDom=this.opts.cDom;
            this.init();
            return this;
        }
        SelectedDom.prototype={
            init:function(){
                this.seleted();
            },
            seleted:function(){
                var self=this;
                $(this.dom).each(function(i){

                var _html,
                    me=$(this),
                    $container=$(self.tDom);
                    me.children(self.cDom).on("touchstart click", function(event){
                        event.stopPropagation();
                        _html=$(this).html();
                        $container.html(_html+'<span class="filt-arrow cur"></span></li>');
                        $($container).addClass('area-cur');
                    });
                    
            });
            }
        }
        new SelectedDom(opts);
    }
})(window,document,window.SY = window.SY || {});

$(function(){
    //右侧滑动展示
    $('.zlgs-tab ul li,.qz-tab ul li,.sbk-tab ul li,.gcs-tab ul li,.czs-tab ul li').on('click',function(event){
        var index=$(this).index();
        event.preventDefault();
        $('#mask1').show();
        $('.found-wrap').eq(index).addClass('is-visible').siblings().removeClass('is-visible');
        $('.found-tab ul li').on('click',function(event){
            event.preventDefault();
            var index=$(this).index();
            $(this).addClass('on').siblings().removeClass('on');
            $('.found-list').children('.found-list-item').eq(index).addClass('focus').siblings().removeClass('focus');
        })
    });
    $('#mask1').on('click',function(event){
        event.preventDefault();
        $('#mask1').hide();
        $('.found-wrap').removeClass('is-visible');
    });  
    
    //擅长
    SY.seletedDom({
        dom:"#list-skill-1 ul",
        tDom:"#list-skill",
        cDom:"#list-skill-1 li"

    })
    //主修
    SY.seletedDom({
        dom:"#list-major-1 ul",
        tDom:"#list-major",
        cDom:"#list-major-1 li"

    })
    //主营
    SY.seletedDom({
        dom:"#list-core-1 ul",
        tDom:"#list-core",
        cDom:"#list-core-1 li"

    })
    //附近
    SY.seletedDom({
        dom:"#list-nearby-1 ul",
        tDom:"#list-nearby",
        cDom:"#list-nearby-1 li"

    })
    //智能排序
    SY.seletedDom({
        dom:"#list-sort-1 ul",
        tDom:"#list-sort",
        cDom:"#list-sort-1 li"

    })
    //设备类型
    SY.seletedDom({
        dom:"#list-area-1 ul",
        tDom:"#list-area",
        cDom:"#list-area-1 li"

    })
    //业务
    SY.seletedDom({
        dom:"#list-business-1 ul",
        tDom:"#list-business",
        cDom:"#list-business-1 li"

    })
    //全部分类
    SY.seletedDom({
        dom:"#list-all-1 ul",
        tDom:"#list-all",
        cDom:"#list-all-1 li"

    })
    
})