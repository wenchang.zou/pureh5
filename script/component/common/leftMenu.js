$(function(){
    //左侧滑动我的菜单
    function myNav(){
        if($("#nav").hasClass("openNav")){
            $("#nav-over").css("display","none");
            $('body').removeClass("layer");
            $("#nav").removeClass("openNav");
        }else{
            $("#nav-over").css("display","block");
            $('body').addClass("layer");
            $("#nav").addClass("openNav");
        }   
    }
    $("#nav-over").on("click",function(){
        $("#nav-over").css("display","none");
        $('body').removeClass("layer");
        $("#nav").removeClass("openNav");
    })
    $("#nav-over").on("touchmove touch",function(e){e.preventDefault()},false);//阴止默认事件
    $(".navLeft").on("click",myNav);

})   