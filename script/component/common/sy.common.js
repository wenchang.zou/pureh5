// JavaScript Document
/*cherry 13030131*/
(function(W,D,M){

    M.bannerImage=function(opts){
        function BannerImage(opts){
            this.opts=opts || {};
            this.myImg=this.opts.myImg || $(".myImg");
            this.logoH=this.opts.logoH || $(".logo-layer-con");
            this.listLen=this.opts.listLen;
            this.slideUl=this.opts.slideUl;
            this.parentUl=this.opts.parentUl || $(".index-slide");
            this.middleCon=this.opts.middleCon || $(".top-middle-con");
        }
        BannerImage.prototype={
            init:function(){
                this.bannerSize();
            },
            bannerSize:function(){
                var len, widths,imgH,logoH,that=this,sizes;
                window.addEventListener("resize",function(){
                    size();
                });

                var size=function(){

                    len=that.listLen.size();
                    sizes=that.parentUl.children("ul").children("li");
                    widths=$(".main-wrap").width() >=640 ? 640 : $(".main-wrap").width();
                    imgH=that.myImg.height();
                    that.slideUl.width(widths*len+"px");
                    that.parentUl.find("img").css({height:($(".main-wrap").width()/640)*220+"px"});

                    that.parentUl.css({height:($(".main-wrap").width()/640)*220+"px"});
                    that.middleCon.height(($(".main-wrap").width()/640)*354);
                    logoH=that.logoH.height(that.middleCon.height()-($(".main-wrap").width()/640)*220);
                    for(var i=0; i<len; i++){
                        that.listLen[i].style.width=widths+"px";
                        that.listLen[i].style.left=widths*i+"px";
                    }

                };
                size();

            }
        }
        return new BannerImage(opts).init();
    };
    M.lazyLoad={
        lazyLoad:function(el){
            if(!document.querySelector(el)){return;}
            var delay = null;
            // var elBox = SN.dEach(el);
            // var arr = [];
            // var tmpImg = SN.find(elBox,"img");
            $(el).find("img").each(function(){
                var _this = $(this)[0];
                if($(this).offset().top<window.innerHeight){
                    _this.onerror = function(){
                        this.src = "http://script.suning.cn/images/ShoppingArea/Common/blank.gif";
                    }
                    _this.src=this.getAttribute("lazy-src");
                    _this.className=_this.className+"bouseIn";
                    _this.setAttribute("lazy-src","finish");
                }
            });
            window.addEventListener("scroll", function(){
                delay = setTimeout(function(){
                    $(el).find("img").each(function(){
                        var _this = $(this)[0];

                        if(_this.getAttribute("lazy-src") == null){
                            return;
                        }
                        var top = $(this).offset().top;

                        var h = window.innerHeight || window.screen.height;
                        if(document.body.scrollTop > top + h || document.body.scrollTop < top - h && _this.getAttribute("lazy-src") != "finish"){

                            clearTimeout(delay);
                            return;
                        }
                        if(document.body.scrollTop > top - h && _this.getAttribute("lazy-src") != "finish"){
                            _this.onerror = function(){
                                this.src = "http://script.suning.cn/images/ShoppingArea/Common/blank.gif";
                            }
                            _this.src=this.getAttribute("lazy-src");
                            _this.className=_this.className+"bouseIn";
                            _this.setAttribute("lazy-src","finish");
                        }
                    })
                },300)

            }, false);
        }
    };


})(window,document,window.SY =window.SY || {});
