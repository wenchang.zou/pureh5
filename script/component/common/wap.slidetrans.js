/**
 *   滑动组件
 *   支持：横向滑动； 特效：缓动，弹簧，碰撞等
 *   @ps: 暂不支持纵向滚动
 *   @desc: 此组件是从网上拷贝下来，找不到原作者，我也不知道贴哪链接地址。我对滑动做了如下优化：
 *              正负方向可无穷移动
 */
(function(w,M){
    var getDom = function (id, isAll) {
        return "string" == typeof id ? (isAll ? document.querySelectorAll(id) : document.querySelector(id)) : id;
    };

    var extend = function(destination, source) {
        for (var property in source) {
            destination[property] = source[property];
        }
        return destination;
    }

    var currentStyle = function(element){
        return element.currentStyle || document.defaultView.getComputedStyle(element, null);
    }

    var bind = function(object, fun) {
        var args = Array.prototype.slice.call(arguments).slice(2);
        return function() {
            return fun.apply(object, args.concat(Array.prototype.slice.call(arguments)));
        }
    }

    var forEach = function (array, callback) {
        for (var i = 0, len = array.length; i < len; i++) {
            callback.call(this, i, array[i]);
        }
    };

    /**
     t：current time当前时间  b：beginning value初始值 c： change in value变化量  d：duration持续时间
     */
    var Tween = {
        Quart: {
            easeOut: function(t,b,c,d){
                return -c * ((t=t/d-1)*t*t*t - 1) + b;
            }
        },
        Back: {
            easeOut: function(t,b,c,d,s){
                if (s == undefined) s = 1.70158;
                return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
            }
        },
        Bounce: {
            easeOut: function(t,b,c,d){
                if ((t/=d) < (1/2.75)) {
                    return c*(7.5625*t*t) + b;
                } else if (t < (2/2.75)) {
                    return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
                } else if (t < (2.5/2.75)) {
                    return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
                } else {
                    return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
                }
            }
        }
    }

    /**
     * 滑动对象
     */
    M.SlideTrans = function(slider, count, options){
        function  SlideTrans(slider, count, options){
            this._slider = getDom(slider);
            this._timer = null;//定时器
            this._count = Math.abs(count);//切换数量
            this._target = 0;//目标值
            this._t = this._b = this._c = 0;//tween参数
            this.Index = 0;//当前索引
            this._accumulationIndex = 0;//累加索引
            this.options = extend(this.options, options || {});
            this._sonDoms = this.options.sonDoms ? getDom(this.options.sonDoms, true) : this._slider.querySelectorAll('li');//滑动子组件列表，默认读li

            this.Auto = !!this.options.Auto;
            this.Duration = Math.abs(this.options.Duration);
            this.Time = Math.abs(this.options.Time);
            this.Pause = Math.abs(this.options.Pause);
            this.Tween = this.options.Tween;
            this.onStart = this.options.onStart;
            this.onFinish = this.options.onFinish;
            this.init();
        }
        SlideTrans.prototype = {
            options: {
                Vertical:	true,//是否垂直方向（方向不能改）
                Auto:		true,//是否自动
                Change:		0,//改变量
                Duration:	50,//滑动持续时间
                Time:		10,//滑动延时
                Pause:		4000,//停顿时间(Auto为true时有效)
                onStart:	function(){},//开始转换时执行
                onFinish:	function(){},//完成转换时执行
                Tween:		Tween.Quart.easeOut//tween算子
            },
            init: function() {
                var bVertical = !!this.options.Vertical;
                this._css = bVertical ? "top" : "left";//方向
                this._cssOffset =  bVertical ? "offsetHeight" : "offsetWidth";

                this._totalSize = this._slider[this._cssOffset];
                this._itemSize = this._sonDoms[0][this._cssOffset];
                this.Change = this.options.Change ? this.options.Change :
                    this._totalSize / this._count;
//                this._mapCss = {};
                this._isForward = true;

                var _this = this;
                this.preData = {
                    itemSize:this._itemSize,                    //项大小
                    totalSize:this._totalSize,  //  总大小
                    accumulationIndex: 0,        //累计索引
                    index: 0
                };//存储前一项的数据信息 itemSize(left/top)
                /*forEach(this._sonDoms,function(k,v){
                 _this._mapCss[k]=parseInt(currentStyle(v)[_this._css]);
                 })*/

            },

            /**
             * 获取匹配的index
             * @private
             */
            _getRuleIndex: function(index){
                index === undefined && (index = this.Index);
//                isForward  = index >= 0;
                index < 0 && (index = this._count-1);
                (index > this._count - 1) && (index = 0);
                return  index;
            },

            //开始切换
            run: function(index) {
                this.Index = this._getRuleIndex(index);

                //方向处理
                this._directionHandler();

                //设置参数
                this._target = this._getTarget(index);
                this._t = 0;
                this._b = parseInt(currentStyle(this._slider)[this._css]);
                this._c = this._target - this._b;
//                this._c = this._isForward ? (-this.Change) : this.Change;

                this.onStart();
                this.move();

                //存储前一项数据信息
                this.preData.itemSize = this._target;
                this.preData.accumulationIndex = this._accumulationIndex;
                this.preData.index = this.Index;
            },
            //移动
            move: function() {
                clearTimeout(this._timer);
                //未到达目标继续移动否则进行下一次滑动
                if (this._c && this._t < this.Duration) {
                    this.moveTo(Math.round(this.Tween(this._t++, this._b, this._c, this.Duration)));
                    this._timer = setTimeout(bind(this, this.move), this.Time);
                }else{
                    this.moveTo(this._target);
                    this.Auto && (this._timer = setTimeout(bind(this, this.next), this.Pause));
                }
            },
            //移动到
            moveTo: function(i) {
                this._slider.style[this._css] = i + "px";
            },
            //下一个
            next: function() {
                this._accumulationIndex++;
                this._isForward = true;
                this.run(++this.Index);
            },
            //上一个
            previous: function() {
                this._isForward = false;
                this._accumulationIndex--;
                this.run(--this.Index);
            },
            //停止
            stop: function() {
                clearTimeout(this._timer); this.moveTo(this._target);
            },

            /**
             * 设置预移动
             */
            setPreMove: function(moveForward){
                this._directionHandler(moveForward);
            },

            _directionHandler: function(moveForward){
                var isForward,liIndex;
                if(moveForward === undefined){
                    isForward = this._isForward;
                    liIndex =  this.Index;
                }else{
                    isForward =  moveForward;
                    liIndex =  this.preData.index;
                    liIndex = this._getRuleIndex(isForward ? ++liIndex : --liIndex);
                }

                var preSize = this.preData.itemSize;
                var addSize = this._itemSize;
                if (isForward){
                    //正向：
                    this._sonDoms[liIndex].style['left'] =  -preSize+addSize+"px";
                }else{
                    this._sonDoms[liIndex].style['left'] =  -preSize-addSize+"px";
                }
            },

            /**
             * 获取挪移位置点
             * @private
             */
            _getTarget: function(){
                //正向：前一个的left值-宽度    负向:前一个的left值+项宽度
                var preSize = this.preData.itemSize;
                var itemSize = this._itemSize;
                return (this._isForward ? preSize-itemSize : preSize + itemSize);
            }
        }
        return new SlideTrans(slider, count, options);
    }
})(window,  window.Wap = window.Wap || {});
