/**
 *   定制滑动组件
 */
;(function($,w,M){
    var slice = Array.prototype.slice;
    var getDom = function (id, isAll) {
        return "string" == typeof id ? (isAll ? document.querySelectorAll(id) : document.querySelector(id)) : id;
    };

    var proxyEvent = function(handler, context){
        return function(e){
            handler.call(context, e);
        }
    };

    //slider: '#J_index_slide .slide_ul
    M.SlideTranStatic = function(slider,leftX,sDom){
        function  SlideTranStatic(slider,leftX,sDom){
            this.slider =  getDom(slider);
            this.LX=parseFloat(leftX)*(-1);
            //坐标
            this._initX = 0;
            this._finishX = 0;
            this._startX = 0;
            this._startY = 0;
            this.initEvent();


        }
        SlideTranStatic.prototype = {



            initEvent: function(){
                var that=this;
                //监听slider touch事件
                this.slider.addEventListener('touchstart', proxyEvent(this.touchStartHandler,this));
                this.slider.addEventListener('touchmove', proxyEvent(this.touchMoveHandler,this));
                this.slider.addEventListener('touchend', proxyEvent(this.touchEndHandler,this));


            },


            touchStartHandler: function(event) {
                this._startX = event.touches[0].clientX;
                this._startY = event.touches[0].clientY;
                this._initX = this._startX;
            },

            touchMoveHandler: function(event) {
                var touches = event.touches;
                var _endX = event.touches[0].clientX;
                var _endY = event.touches[0].clientY;
                if (Math.abs(_endY - this._startY) > Math.abs(_endX - this._startX)) {
                    return;
                }
                event.preventDefault();
                this._finishX = _endX;
                this._startX = _endX;
            },
            touchEndHandler: function(event) {
                if (this._finishX == 0) {
                    return;
                }
                if (this._initX > this._finishX) {
                    this.pagination(this._initX, this._finishX);
                } else if (this._initX < this._finishX) {
                    this.pagination(this._initX, this._finishX);
                }
                this._initX = 0;
                this._finishX = 0;
            },
            pagination: function(start, end) {
                var that=this;
                if (start >= end) {

                    $(slider).children("ul").each(function(){
                        $(this).css('left',that.LX+'rem');
                    });
                    $(slider).siblings(sDom).children("ul").css('left',0);
                } else {
                    $(slider).children("ul").each(function(){
                        $(this).css('left',0);
                    });

                }
            }
        };
        return new SlideTranStatic(slider,leftX,sDom);
    };
})(Zepto,window,  window.SY = window.SY || {});

