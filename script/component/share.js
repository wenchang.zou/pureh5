/* 
 * @Author: 12072542
 * @Date:   2014-05-04 11:56:38
 * @Last Modified by:   luoyuting
 * @Last Modified time: 2014-08-05 17:32:38
 */

;(function($,window,M) {
    M.pageShare=function(options){
        function PageShare(options) {
            this.options = options || {};
            this.linkUrl = this.options.linkUrl || inindow.location.href;
            this.imgUrl = this.options.imgUrl||"";
            this.shareTitle = this.options.shareTitle || "";
            this.souhuTitle = this.options.souhuTitle || "";
            this.descContent = this.options.descContent || "";
            this.pict = "";
            this.appid = "";
            this._initShare();
            return this;
        }
        PageShare.prototype = {
            constructor: PageShare,
            _initShare: function() {
                var that=this;
                // 当微信内置浏览器完成内部初始化后会触发WeixinJSBridgeReady事件。
                document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
                    // 发送给好友
                    WeixinJSBridge.on('menu:share:appmessage', function(argv) {
                        that._shareFriend();
                    });
                    // 分享到朋友圈
                    WeixinJSBridge.on('menu:share:timeline', function(argv) {
                        that._shareTimeline();
                    });
                    // 分享到微博
                    WeixinJSBridge.on('menu:share:weibo', function(argv) {
                        that._shareWeibo();
                    });
                }, false);

                document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
                    WeixinJSBridge.call('hideToolbar');
                });

                that._shareWb();
            },

            //微信分享
            _shareFriend: function() {
                var that = this;
                WeixinJSBridge.invoke('sendAppMessage', {
                    "appid": that.appid,
                    "img_url": that.imgUrl,
                    "img_width": "200",
                    "img_height": "200",
                    "link": that.linkUrl,
                    "desc": that.descContent,
                    "title": that.shareTitle
                }, function(res) {
                    //_report('send_msg', res.err_msg);
                });
            },

            _shareWeibo: function() {
                var that = this;
                WeixinJSBridge.invoke('shareWeibo', {
                    "content": that.descContent,
                    "url": that.linkUrl
                }, function(res) {
                    //_report('weibo', res.err_msg);
                });
            },

            _shareTimeline: function() {
                var that = this;
                WeixinJSBridge.invoke('shareTimeline', {
                    "img_url": that.imgUrl,
                    "img_width": "200",
                    "img_height": "200",
                    "link": that.linkUrl,
                    "desc": that.descContent,
                    "title": that.shareTitle
                }, function(res) {
                    //_report('timeline', res.err_msg);
                });
            },
            _shareWb: function() {
                var that=this;
                var appkey = encodeURI('65e3731f449e42a484c25c668160b355');
                var pic = ""; //encodeURI($("#picUrl").val());
                var site = encodeURI('http://www.suning.com');
                var u = 'http://v.t.qq.com/share/share.php?title=' + that.shareTitle + '&url=' + that.linkUrl + '&appkey=' + appkey + '&site=' + site + '&pic=' + that.pict;

                $(".share_weibo").attr("href",'http://v.t.sina.com.cn/share/share.php?url='+that.linkUrl+'&appkey=400813291&title='+that.shareTitle+'&pic=');
                $(".share_kaixin").attr("href", 'http://www.kaixin001.com/rest/records.php?url=' + that.linkUrl + '&style=11&content=' + that.shareTitle + '&pic=' + that.pict + '&stime=&sig=');
                $(".share_douban").attr("href", 'http://www.douban.com/recommend/?url=' + that.linkUrl + '&title=' + that.shareTitle + '&comment=' + encodeURI(that.shareTitle));
                $(".share_renren").attr("href", 'http://widget.renren.com/dialog/share?resourceUrl=' + that.linkUrl + '&title=' + encodeURI(that.linkUrl) + '&description=' + encodeURI(that.shareTitle));
                $(".share_qqweibo").attr("href", u);
                $(".share_sohu").attr("href", 'http://t.sohu.com/third/post.jsp?&url=' + that.linkUrl + '&title=' + that.souhuTitle + '&content=utf-8&pic=' + that.pict);
                var p = {
                    url: that.linkUrl,
                    desc: '',
                    summary: '',
                    title: that.shareTitle,
                    site: '三一租赁平台',
                    pics: that.pict
                };
                var s = [];
                for (var i in p) {
                    s.push(i + '=' + encodeURIComponent(p[i] || ''));
                }
                $(".share_qzone").attr("href", 'http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?' + s.join('&'));
            }
        };
        new PageShare(options);
    }


})(Zepto,window, window.SY=Window.SY || {});

/*$(function(){
    $(".shareLinks").on("click",".shareLink", function(){
            var shareThis = new pageShare({
                //分享的标题
            shareTitle:"分享标题"
        });   
    });
    //微信分享操作
    var shareThis = new pageShare({
        //分享的标题
        shareTitle:"分享标题",
        //微信分享的内容概要
        descContent:"分享内容",
        //微信分享的缩略图的地址
        imgUrl:"images/logo2.jpg"
    });
});*/



