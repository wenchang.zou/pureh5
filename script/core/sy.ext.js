/**
 * HB项目扩展工具类
 * @author *
 * TODO: id拆分工具类？ 如果传递了ID，则在加载工具类基类的基础上，
 */
;
(function ($, M, W) {
  var slice = Array.prototype.slice;

  /** ECMA扩展  **/
  /**
   *  格式化:支持 动态参数/数组/json对象
   * @desc xxx{0}xx{1} --> a,b --> xxxaxxb  或 xxx{a}xx{b} --> {a:1,b:2} --> xxx1xx2
   * @returns
   */
  !String.format && (String.prototype.format = function () {
    var jsonFlag = arguments.length == 1 && arguments[0] instanceof Object,
      args = jsonFlag ? arguments[0] : arguments,
      reg = jsonFlag ? /\{(\w+)\}/g : /\{(\d+)\}/g;

    return this.replace(reg,
      function (m, i) {
        return args[i] || '';
      });
  });

  /**
   * @desc 替换全部
   * @param oldStr   String/RegExp
   */
  String.prototype.replaceAll = function (oldStr, newStr) {
    if (!RegExp.prototype.isPrototypeOf(oldStr)) {
      return this.replace(new RegExp(oldStr, 'g'), newStr);
    } else {
      return this.replace(oldStr, newStr);
    }
  };

  /** 项目扩展类  **/
  W.$H = M.Util = {


    /**
     * 回调队列,不支持异步
     * TODO:后续需优化此方法，参照$.Deffered
     */
    deferred: function () {
      var args = slice.call(arguments, 0);

      return function (data) {
        for (var i = 0, len = args.length; i < len; i++) {
          if (!args[i](data)) break;
        }
      };
    },

    onlyClass: function (dom, className) {
      dom.addClass(className).siblings().removeClass(className);
    },

    //屏蔽事件
    isNoEvent: function ($dom) {
      return $dom.hasClass('hb-no-event');
    },

    /**
     * 2014-07-09 00:00:00.0 转换成时间对象  {y: m: d: h: mi: s:}
     */
    getDate: function (date) {
      try {
        //时间格式可能是 2014-07-09 00:00:00.0。需去掉.0
        var index = date.indexOf('.');
        (index != -1) && (date = date.substring(0, index));

        var arr = date.split(' '),
          ymd = arr[0].split('-'),
          time = arr[1].split(':');

        return {
          y: ymd[0],
          m: ymd[1],
          d: ymd[2],
          h: time[0],
          mi: time[1],
          s: time[2]
        }
      } catch (e) {
        return {
          y: 1, m: 1, d: 1, h: 1, mi: 1, s: 1
        }
      }
    },

    isMobile: function () {
      return (navigator.userAgent.match(/Win/i)
      || navigator.userAgent.match(/MacIntel/i)
      || navigator.userAgent.match(/Linux/i)
      ) ? false : true;
    },
    isPhone: function () {
      return /(iPhone|iPod|iPad);?/i.test(navigator.userAgent);
    },
    isAndriod: function () {
      return /android/i.test(navigator.userAgent);
    },

    /**
     * TO 销售列表
     */
    toSaleList: function () {

      this.redirect(M.Const.SALE_PATH);
    },
    /**
     *  心跳方法
     *  @param waitSecond秒数   fn：每次的执行方法    backFn  退出时的方法
     *
     */
    setInterval: function (waitSecond, fn, backFn) {
      var wait = waitSecond;

      //设置秒数(单位秒)
      var i = 0,
        ind = '',
        timer = function () {
          var r = wait - i;
          if (r == 0) {
            clearInterval(ind);
            backFn && backFn();
          } else {
            fn && fn(r);
            i++;
          }
        };

      ind = setInterval(timer, 1000);
    },


    /**
     *获取URL参数
     */
    getURLParam: function () {
      var search = location.search,
        reg = /[^\&]+=[^\&]+/g;

      if (!search) return {};

      //解析出锚点参数
      search = search.slice(1);

      var arr, arrs, result = {};
      while (arr = reg.exec(search)) {
        if (arrs = arr[0].match(/[^\=]+/g)) {
          result[arrs[0]] = arrs[1];
        }
      }

      return result;
    },
    /**
     * @desc tab 选项卡方法
     * @desc tit 选项卡标题
     * @desc cont:tab所指向的层
     */

    tabMethod: function (tit, cont, className) {
      var i = $(tit).index();
      $(tit).addClass(className).siblings().removeClass(className);
      $(cont).eq(i).show().siblings(cont).hide();
    },
    toRegister: function () {

      this.redirect(M.Const.REG_PATH);
    },

    errorText: function (errLay, dom, errorTxt) {
      if (dom.val() == "" || dom.val() == null) {
        dom.parent("li").next(errLay).text(errorTxt).show();
        dom.css({
          "border": "#f50707 1px solid"
        });
      } else {
        dom.css({
          "border": "#d2d2d2 1px solid"
        });
        dom.parent("li").next(".the-warn-lay").hide();
      }
    },


    /**
     * 携带openId页面跳转
     */
    redirectId: function (url, json) {
      //取openID
      this.redirect(url, $.extend({}, json, {openid: M.Data.OPEN_ID}));
    },

    /**
     * 跳转： 只传递url，则采用location.href, 传递json，则'?A=B&A=B'拼接
     */
    redirect: function (url, json) {
      url ? (location.href = json ? this.addURLParam(url, json) : url) :
        location.reload();
    },

    tipNetError: function () {
      try {
        WP.ErrorNet();
      } catch (e) {
        this.tipError('网络异常，请稍后再试');
        //console.error('Component Wap.ErrorNet is not exist');
      }
//			this.tipError( '网络异常，请稍后再试');

    },
    tipError: function (msg) {
      WP.alertBox({type: "mini", msg: msg});
    },

    /**
     * 给URL增加参数
     */
    addURLParam: function (url, param) {
      var p = param || {},
        noP = url.indexOf('?') === -1,
        tpl = '&{0}={1}',
        params = '';
      $.each(param, function (k, v) {
        params += tpl.format(k, v);
      });

      noP && (params = params.replace('&', '?'));

      return url + params;
    },
    zeroChanged: function (param) {
      for (var key in param) {
        if (param[key] == undefined || param[key] == 0 || param[key]==0.00 || param[key]==null) {
          param[key] = "0";
        }
      }
    },

    /**
     * 显示或隐藏  true：显示  false：隐藏
     */
    toggle: function ($d, flag, conf) {
      var showFn, hideFn;

      if (conf) {
        showFn = conf.show;
        hideFn = conf.hide;
      }

      if (flag) {
        this.show($d);
        showFn && showFn();
      } else {
        this.hide($d);
        hideFn && hideFn();
      }

    },

    /**
     * 日志
     */
    error: function (msg) {
      var c = W.console;
      if (!c) {
        c = {};
        c.error = function () {
        };
      }
      c.error(msg);
    },


    hide: function (dom) {
      dom.style.display = 'none';
    },

    show: function (dom) {
      dom.style.display = 'block';
    },
    adaptHeight: function () {
      var winHeight = parseInt(W.innerHeight),
        bodyHeight = parseInt(document.body.clientHeight + $(".get-hb-foot").height()),
        docHeight = Math.max(winHeight, bodyHeight),
        sections = document.querySelector("section"),
        sArr = sections.classList;
      sArr = Array.prototype.slice.call(sArr);
      sArr.push("pd-hb-btm");
      var sNArr = sArr.join(' ');
      //设置值使得背景图片可以适应窗口的高度
      sections.style.height = docHeight + "px";
      sections.style.backgroundSize = "20rem " + docHeight + "px";
      (winHeight <= bodyHeight) && (sections.className = sNArr);

    },
    toggleClass: function (dom, sib, className) {
      dom.addClass(className).siblings(sib).removeClass(className);
    },
    textChange: function (dom, tDom, li, url, backFn) {
      var self = this, masks = document.getElementById("tMask");
      tDom.show();
      this.theMask();
      tDom.children(li).each(function (i) {
        $(this).on(M.EVENT.CLICK, function () {
          dom.html($(this).text()).show();
          $H.redirect(url, {merchantId: $(this).attr("data-merchantId"), storeId: $(this).attr("data-storeId")});
          backFn && backFn();
          self.removeEl(masks);
          tDom.hide();
        });
      });

    },
    theMask: function () {
      var masks = document.createElement("div"),
        h = document.documentElement.scrollHeight > window.innerHeight ? document.documentElement.scrollHeight : window.innerHeight;

      masks.setAttribute("id", "tMask");
      masks.style.cssText = "position:absolute;left:0;top:0;width:100%;height:" + h + "px;background:rgba(0,0,0,0.3);z-index:1";
      document.body.appendChild(masks);
    },
    removeEl: function (el) {
      document.body.removeChild(el);
    },
    isAllowedPwdSee: function (seeClass, dom, input) {

      if (input.attr("type") == "password") {
        dom.addClass(seeClass);
        input.attr("type", "text");

      } else {
        dom.removeClass(seeClass);
        input.attr("type", "password");
      }
    },
    theDateToDom: function (type, sDom, eDom) {
      var dates = new Date(),
        years = dates.getFullYear(),
        months = dates.getMonth() + 1, //月份是从0开始计算
        date = dates.getDate(),
        day = (dates.getDay() == 0) ? 6 : dates.getDay() - 1,
        dday = date - day;

      months = (months < 10) ? ("0" + months) : months;
      date = (date < 10 ) ? ( "0" + date) : date;
      dday = (dday < 10 ) ? ( "0" + dday) : dday;
      switch (type) {
        case "date":
          this.isDomSee(true);
          sDom.html(years + "-" + months + "-" + date);
          eDom.html(years + "-" + months + "-" + date);

          return !1;
        case "week":
          this.isDomSee(true);
          sDom.html(years + "-" + months + "-" + dday);
          eDom.html(years + "-" + months + "-" + date);

          return !1;
        case "month":
          this.isDomSee(true);
          sDom.html(years + "-" + months + "-" + "01");
          eDom.html(years + "-" + months + "-" + date);

          return !1;
        case "self":
          this.isDomSee(false);
          return !1;

      }


    },
    isDomSee: function (isDate) {
      if (isDate) {
        $(".the-data-txt").show();
        $(".the-self-define").hide();
      } else {
        $(".the-data-txt").hide();
        $(".the-self-define").show();

      }
    },
    todayFun: function () {
      var dates = new Date(),
        years = dates.getFullYear(),
        months = dates.getMonth() + 1, //月份是从0开始计算
        date = dates.getDate();
      months = (months < 10) ? ("0" + months) : months;
      date = (date < 10 ) ? ( "0" + date) : date;
      return (years + "-" + months + "-" + date);
    }
  }
})(Zepto, window.SY = window.SY || {}, window);
