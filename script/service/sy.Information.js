/**
 *
 */
;(function($,M){
    var names;
    function init(){
        flushData();
    }
    function initEvent(){
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){
                case "nick-btn":$H.redirect("mySubInfo.html",{titleName:$(".nick").text(),type:"nick"});return !1;
                case "sexy-btn":$H.redirect("mySubInfo.html",{sexyName:$H.getURLParam().sexyName,type:"sexy"});return !1;
                case "account-btn":$H.redirect("mySubInfo.html",{accountName:$(".account").text(),type:"account"});return !1;
            }
        });
    }

    function flushData(){
        theModifyCon(false, ".nick");
        theModifyCon(false, ".account");
        theModifyCon(true,".sexy");

    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){

        SY.uploadPreview({
            width:"3.5rem",
            height:"3.5rem",
            imgsDiv:".modify-rig",
            fileCon:$(".file-con")

        });

    }
    function theModifyCon(isSexy,dom,sDom){
        if(isSexy){
            names=window.sessionStorage.sexyType;
            switch(names){
                case "0" : $(dom).text("男"); return !1;
                case "1" : $(dom).text("女"); return !1;
            }
        }else{
            switch(dom){
                case ".nick": names=window.sessionStorage.titleName !=undefined ? window.sessionStorage.titleName :"Amn.";
                            $(dom).text(names); return !1;
                case ".account": names=$H.getURLParam().accountName !=undefined ? $H.getURLParam().accountName :"13528212323";
                      $(dom).text(names);return !1;
                default:
                    names=$H.getURLParam().titleName;
                }

        }


    }
    $(function(){
        preCreate();
        create();
    });
})(Zepto, window.SY=window.SY || {});
