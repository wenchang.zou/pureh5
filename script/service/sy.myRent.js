/**
 * Created by cherry on 2015/4/29.
 */
;(function($,M){
    var
        $rentMain=$(".rent-main-con"),
        TPL_CON=[
            '<div class="rent-pare">',
                '<ul class="rent-list">',
                    '<li class="list-left"  data-rent-id="{rentId}">',
                        '<div class="the-tit">{proName}',

                            '<span class="mark-txt {isGray}">{isRegister}</span>',
                        '</div>',

                        '<div class="sub-title">',
                            '<span class="sub-left">{amount}</span>',
                            '<span class="sub-right">{theDate}</span>',
                        '</div>',

                        '<div class="price-con">{priceCon} <span class="price-unit">元/台</span></div>',
                    '</li>',
                    '<li class="hide-area">',
                        '<div class="update-btn" data-event-name="update-btn"  data-rent-id="{rentId}">更新求租</div>',
                        '<div class="down-btn" data-event-name="off-btn">下架</div>',
                    '</li>',
                '</ul>',
        '</div>'
        ].join('');
    function init(){
        flushData();
    }
    function initEvent(){
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){
                case "my-btn": $H.redirect("myInformation.html");return !1;

            }
        });
    }
    function toUpdate(dom,url){
        var $rentId;
        $(dom).each(function(){
            $(document).on(M.EVENT.CLICK, dom, function(e){
                $rentId=$(this).attr("data-rent-id");
                $H.redirect(url,{rentId:$rentId});
            });
        });
    }

    function flushData(){
        var len, arr=[],items;
        M.Req.rentReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                items=data[i];
                arr.push(TPL_CON.format(items));
            }
            $rentMain.append(arr.join(''));
            preCreate();
            toUpdate(".list-left","myQzDetail.html");
            toUpdate(".update-btn","myQzUpdate.html");

        });

    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        var len=document.querySelectorAll(".rent-pare").length,
            rentP=document.querySelectorAll(".rent-pare");
        for(var i=0; i<len; i++){
            rentP[i].setAttribute('id','J_index_slide'+i);
            SY.SlideTranStatic('#J_index_slide'+i,'10','.rent-pare');
        }
    }
    $(function(){

        create();
    });
})(Zepto, window.SY=window.SY || {});
