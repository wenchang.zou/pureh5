/**
 * Created by cherry on 2015/4/29.
 * @desc listClick 城市级联二级方法，点击省份时，出现市级，
 * @desc listClickSub 城市级联三级方法， 点击市级时，出现区级
 * @desc listClickEq 设备级联方法，点击一级级联方法，渲染该级别的子分类
 * @desc areasLi 最后级别方法，主要作用的是选择之后往sessionStorage里面塞最后一级的数据
 * @desc toPlaceHolder 把sessionStorage里面数据取出来，放进到input的placeHolder里面
 * @desc imgToLayer 把上传的图片放入指定的层里面
 */
;(function($,M){
    var
        $imgHeader=$("#upload-sign"),
        $Certificate=$("#upload-list-1"),
        $theExample=$("#upload-list-2"),
        IMG_CON=[
            '<li class="upload-item items{idx}">',
                '<a href="javsacript:;" class="upload-delete del{idx}" title="删除"></a>',
                '<div class="img-div{idx}"></div>',
            '</li>'
        ].join(''),
        session=window.sessionStorage;
    function init(){
        flushData();

    }
    function initEvent(){

        $(document).on(M.EVENT.CLICK,function(e){

            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){

                case "submit-btn":
                    session.id2=2;
                    $H.setInterval(2,function(){
                        SY.alertBox({
                            type:"mini",
                            msg:"恭喜您,注册租赁公司成功"
                        })
                    },function(){

                        $H.redirect("myHome.html",{id2:2,onName:"on"});
                    });return !1;

            }

        });

    }

   function flushData(){

   }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        imgToLayer("#fileImage",1,".upload-delete",".upload-item",$imgHeader);
        imgToLayer("#fileImage-1",3,".upload-delete",".upload-item",$Certificate);
        imgToLayer("#fileImage-2",3,".upload-delete",".upload-item",$theExample);


    }


    // 上传图片 最多只可以上传三张图片
    function imgToLayer(fileDom,type,del,pare,pares){
        var i=0,arr=[];
            $(document).on(M.EVENT.CLICK, fileDom, function(){
                switch(type){
                    case 1:
                        if(pares.children(pare).size()<=0){
                            $(fileDom).attr("class","file0");
                            pares.empty();
                            arr.push(IMG_CON.format({idx:"0"}));
                            pares.append(arr.join(''));
                            SY.uploadPreview({
                                width:"4.25rem",
                                height:"4.25rem",
                                imgsDiv:".img-div0",
                                fileCon:$(".file0")
                            });
                        }

                        return !1;
                    case 3:
                       i++;
                        if(i<=3){
                            $(fileDom).attr("class","file"+i);
                            pares.empty();
                            arr.push(IMG_CON.format({idx:i}));
                            pares.append(arr.join(''));
                            SY.uploadPreview({
                                width:"4.25rem",
                                height:"4.25rem",
                                imgsDiv:".img-div"+i,
                                fileCon:$(".file"+i)
                            });
                        }



                        return !1;
                }


                $(del).on(M.EVENT.CLICK,function(){
                    $(this).parent(pares)[0].style.display="none";
                    i=i-1;
                })

            });



    }
    $(function(){
        preCreate();
        create();
    });


})(Zepto, window.SY=window.SY || {});
