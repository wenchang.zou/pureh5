/**
 * SY 项目基类
 * @author *
 * @desc  公共常量申明；事件源申明；
 *
 */

(function(M){

   /**  通用数据  **/
   M.Data = {
	    IS_MOBILE: $H.isMobile(),
		  URL_PARAM: $H.getURLParam()
   }


   /**  事件兼容  **/
   M.EVENT = {
		 CLICK: M.Data.IS_MOBILE ? 'tap' : 'click'
   }
  /**  样式兼容  **/
  M.STYLE = {
    UN_EVENT: 'wp-no-event'
  }

  /**  通用数据扩展  **/
   $.extend(M.Data, {
       CODE: M.Data.URL_PARAM.code,
          OPEN_ID: M.Data.URL_PARAM.openid,
        WXUID: M.Data.URL_PARAM.weixinUid,
        STORE_ID: M.Data.URL_PARAM.storeId,
        MERCHANT_ID: M.Data.URL_PARAM.merchantId,
        SHOP_NAME: M.Data.URL_PARAM.shopName,
        START: M.Data.URL_PARAM.startDate,
        END: M.Data.URL_PARAM.endDate,
        ID: M.Data.URL_PARAM.id,
        ALIPAYID: M.Data.URL_PARAM.alipayApplyId

   });

   /**  测试  **/
   window.test = function(msg){
	   if(M.Const.IS_TEST){
		   msg instanceof Function ? msg() : alert(msg);
	   }
   }

})(window.SY = window.SY || {});
