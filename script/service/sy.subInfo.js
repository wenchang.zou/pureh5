/**
/**
 * Created by cherry on 2015/4/29.
 */
;(function($,M){
    var MOD_CON=['修改{modifyType}'].join(''),
        $modifyType=$(".modify-type"),
        $mainCon=$(".sub-per-info").children(".modify-list"),
        types=$H.getURLParam().type,
        $sexy=$(".sexy-con"),
        sexyType;

    function init(){
        flushData();
    }
    function initEvent(){

        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){


            }
        });

    }
    function sexyDefault(){
        $sexy.each(function(i){
            var $this=$(this),
                idx;

            $(this).on(M.EVENT.CLICK, function(){
                idx=$this.index();
                window.sessionStorage.sexyType=idx;
                $H.getURLParam().sexyName=window.sessionStorage.sexyType;
                $this.addClass("on").siblings(".sexy-con").removeClass("on");
                window.setInterval(function(){
                    $H.redirect("myInformation.html",{
                        sexyName: window.sessionStorage.sexyType,
                        titleName: window.sessionStorage.titleName,
                        accountName:window.sessionStorage.accountName
                    });
                },2000)
            });

        });
    }

    function flushData(){
        var types=$H.getURLParam().type;
        switch(types){
            case "nick":inputDom(0,"昵称");return !1;
            case "sexy":inputDom(1,"性别");return !1;
            case "account":inputDom(2,"账户");return !1;
        }

    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        sexyDefault();
    }


    function inputDom(idx,name){
        var $ipt=$mainCon.children("li").eq(idx).children("input"),
            $ipts= $mainCon.children("li").eq(idx).siblings("li").children("input"),
            value;
            MOD_CON.format({
                modifyType:name
            });
            $modifyType.html( MOD_CON.format({
                modifyType:name
            }));
            $mainCon.children("li").eq(idx)[0].style.display="block";
            $mainCon.children("li").eq(idx).siblings("li").hide();

            if(idx==0){
                window.sessionStorage.titleName=$H.getURLParam().titleName;
                $ipt.on("change blur",function(){
                    window.sessionStorage.titleName=$(this).val();
                    $H.redirect("myInformation.html",{
                        titleName:$(this).val(),
                        accountName:window.sessionStorage.accountName,
                        sexyName: window.sessionStorage.sexyType
                    });
                });
                $ipt.val(window.sessionStorage.titleName);
            }else if(idx==1){
                sexyType=$H.getURLParam().sexyName;
                window.sessionStorage.sexyType=sexyType;
                switch(sexyType){
                    case "0": $sexy.eq(0).addClass("on").siblings(".sexy-con").removeClass("on");return !1;
                    case "1": $sexy.eq(1).addClass("on").siblings(".sexy-con").removeClass("on");return !1;
                }
            }else if(idx==2){

                window.sessionStorage.accountName=$H.getURLParam().accountName;
                $ipt.on("change blur",function(){
                    if(!(/^(13|15|18|17)(\d{9})$/g).test($(this).val())){
                        SY.alertBox({
                            type:"mini",
                            msg:"手机号码不正确"
                        })
                    }else{
                        window.sessionStorage.accountName=$(this).val();
                        $H.redirect("myInformation.html",{
                            titleName: window.sessionStorage.titleName,
                            accountName:$(this).val(),
                            sexyName: window.sessionStorage.sexyType
                        });
                    }

                });
                $ipt.val(window.sessionStorage.accountName);
            }

    }
    $(function(){
        preCreate();
        create();
    });
})(Zepto, window.SY=window.SY || {});
