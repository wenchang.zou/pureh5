/**
 * Created by cherry on 2015/4/29.
 * @desc listClick 城市级联二级方法，点击省份时，出现市级，
 * @desc listClickSub 城市级联三级方法， 点击市级时，出现区级
 * @desc listClickEq 设备级联方法，点击一级级联方法，渲染该级别的子分类
 * @desc areasLi 最后级别方法，主要作用的是选择之后往sessionStorage里面塞最后一级的数据
 * @desc toPlaceHolder 把sessionStorage里面数据取出来，放进到input的placeHolder里面
 * @desc imgToLayer 把上传的图片放入指定的层里面
 */
;(function($,M){
    var $provice=$("#leftSelect"),
        $eqiup=$("#leftSelect2"),
        $year=$("#leftSelect5"),
        $address=$("#floatArea"),
        $addrIpt=$("#szd-selectval"),
        $mask=$("#nav-over"),
        $opIpt=$("#czjx-selectval"),
        $oparate=$("#floatArea2"),
        $yearIpt=$("#cynx-selectval"),
        $yearCon=$("#floatArea5"),
        $imgHeader=$("#upload-head"),
        $Certificate=$("#upload-list-1"),
        TPL_CON=[
            '<li data-parent="{parent}" data-id="{id}">{name}</li>'
        ].join(''),
        YEAR_CON=[
            '<li data-year="{yearId}">{years}</li>'
        ].join(''),
        IMG_CON=[
            '<li class="upload-item items{idx}">',
                '<a href="javsacript:;" class="upload-delete del{idx}" title="删除"></a>',
                '<div class="img-div{idx}"></div>',
            '</li>'
        ].join(''),
        sessions=window.sessionStorage;
    function init(){
        flushData();
        flushEquip();
        flushYear();
    }
    function initEvent(){
        listClick("#leftSelect","#centerSelect","#rightSelect");
        listClickEq("#leftSelect2","#rightSelect2");
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){

                case "submit-btn":
                    sessions.ids="0";
                    $H.setInterval(2,function(){
                        SY.alertBox({
                            type:"mini",
                            msg:"恭喜您,注册操作手成功"
                        })
                    },function(){

                        $H.redirect("myHome.html",{ids:"0",onName:"on"})
                    });return !1;

            }

        });

    }
    //渲染一级所在地址，省份
    function flushData(){
        var arr=[], len, item;
        M.Req.cityReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                item=data[i];
                if(item.parent==0){
                    arr.push(TPL_CON.format(item));
                }

            }
            $provice.append(arr.join(''));

        });
    }
    //渲染一级设备名称
    function flushEquip(){
        var arr=[], len, item;
        M.Req.eqClassReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                item=data[i];
                if(item.parent==0){
                    arr.push(TPL_CON.format(item));
                }

            }
            $eqiup.append(arr.join(''));

        });
    }
    //渲染年份
    function flushYear(){
        var arr=[], len, item;
        M.Req.yearReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                item=data[i];
                arr.push(YEAR_CON.format(item));
            }
            $year.append(arr.join(''));

            areasLi($year,"year");

        });
    }
    // 点击所在地一级菜单
    function listClick(dom,toDom,cDom){
        $(dom).children("li").each(function(i){
           var $this=$(this);
            $(this).on(M.EVENT.CLICK,function(){
                $(this).addClass("on-style").siblings("li").removeClass("on-style");
                var arr=[], len, item;
                sessions.province=$(this).text();

                sessions.citys='';
                M.Req.cityReq(function(data){
                    len=data.length;
                    for(var j=0; j<len; j++){
                        item=data[j];
                        if(item.parent==$this.attr("data-id")){
                            console.log($this.attr("data-id"));
                            arr.push(TPL_CON.format(item));
                        }
                        $(toDom).empty();
                        $(cDom).empty();
                        $(toDom).append(arr.join(''));
                        listClickSub("#centerSelect","#rightSelect");
                    }

                });
            })
        });

    }
    //点击所在地二级页面
    function listClickSub(dom,toDom){
        $(dom).children("li").each(function(i){
            var $this=$(this);
            $(this).on(M.EVENT.CLICK,function(){
                var arr=[], len, item;
                $(this).addClass("on-style").siblings("li").removeClass("on-style");
                sessions.area='';
                sessions.citys=$(this).text();
                M.Req.cityReq(function(data){
                    len=data.length;
                    for(var j=0; j<len; j++){
                        item=data[j];
                        if(item.parent==$this.attr("data-id")){
                            arr.push(TPL_CON.format(item));
                        }
                        $(toDom).empty();
                        $(toDom).append(arr.join(''));
                        areasLi(toDom,"area");
                    }
                });
            })
        });

    }
    //点击设备，出现二级设备
    function listClickEq(dom,toDom){
        $(dom).children("li").each(function(i){
            var $this=$(this);
            $(this).on(M.EVENT.CLICK,function(){
                $(this).addClass("on-style").siblings("li").removeClass("on-style");
                var arr=[], len, item;

                sessions.pareEq=$(this).text();

                M.Req.eqClassReq(function(data){
                    len=data.length;
                    for(var j=0; j<len; j++){
                        item=data[j];
                        if(item.parent==$this.attr("data-id")){
                            arr.push(TPL_CON.format(item));
                        }
                        $(toDom).empty();
                        $(toDom).append(arr.join(''));

                        areasLi(toDom,"sonEq");

                    }
                });
            })
        });

    }

    function areasLi(dom,type){
        $(dom).children("li").each(function(k){
            $(this).on(M.EVENT.CLICK,function(){
                $mask[0].style.display="none";
                $(this).addClass("on-style").siblings("li").removeClass("on-style");
                switch(type){
                    case "area" :sessions.area=$(this).html();
                        toPlaceHolder($addrIpt,"prov");
                        $address[0].style.display="none";
                        return !1;
                    case "sonEq" :
                        sessions.sonEq=$(this).html();
                        toPlaceHolder($opIpt,"equip");
                        $oparate[0].style.display="none";
                        return !1;
                    case "year" :
                        sessions.year=$(this).html();
                        toPlaceHolder($yearIpt,"year");
                        $yearCon[0].style.display="none";

                        return !1;
                }

            });
        });
    }
    function toPlaceHolder(doms,types){

        switch(types){
            case "prov" : $(doms).attr("placeholder", sessions.province+" "+sessions.citys+" "+sessions.area); return !1;
            case "equip" : $(doms).attr("placeholder", sessions.pareEq+" "+sessions.sonEq); return !1;
            case "year" : $(doms).attr("placeholder", sessions.year); return !1;

        }
    }



    function clickLayer(dom, sDom, sDom1,cDom, clsName,type){
        $(document).on(M.EVENT.CLICK, dom, function(){
            $(sDom).show();
            $(sDom1).show();
            $('body').addClass(clsName);
        });
        $(document).on(M.EVENT.CLICK, cDom, function(){
            $(sDom).hide();
            $(sDom1).hide();
            $('body').removeClass(clsName);
            toPlaceHolder(dom,type);
        });

    }

    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        theLayOut();
        imgToLayer("#fileImage",1,".upload-delete",".upload-item",$imgHeader);
        imgToLayer("#fileImage-1",3,".upload-delete",".upload-item",$Certificate);
        imgToLayer("#fileImage-1",3,".upload-delete",".upload-item",$Certificate);
        imgToLayer("#fileImage-1",3,".upload-delete",".upload-item",$Certificate);

    }


    // 上传图片 最多只可以上传三张图片
    function imgToLayer(fileDom,type,del,pare,pares){
        var i=0,arr=[];
            $(document).on(M.EVENT.CLICK, fileDom, function(){
                switch(type){
                    case 1:
                        if(pares.children(pare).size()<=0){
                            $(fileDom).attr("class","file0");
                            pares.empty();
                            arr.push(IMG_CON.format({idx:"0"}));
                            pares.append(arr.join(''));
                            SY.uploadPreview({
                                width:"4.25rem",
                                height:"4.25rem",
                                imgsDiv:".img-div0",
                                fileCon:$(".file0")
                            });
                        }

                        return !1;
                    case 3:
                       i++;
                        if(i<=3){
                            $(fileDom).attr("class","file"+i);
                            pares.empty();
                            arr.push(IMG_CON.format({idx:i}));
                            pares.append(arr.join(''));
                            SY.uploadPreview({
                                width:"4.25rem",
                                height:"4.25rem",
                                imgsDiv:".img-div"+i,
                                fileCon:$(".file"+i)
                            });
                        }



                        return !1;
                }


                $(del).on(M.EVENT.CLICK,function(){
                    $(this).parent(pares)[0].style.display="none";
                    i=i-1;
                })

            });



    }
    $(function(){
        preCreate();
        create();
    });
    // 弹出框特效
    function theLayOut(){
        //所在地
        clickLayer('#szd-selectval','#nav-over','#floatArea','.cancel1','layer',"prov");
        clickLayer('#szd-selectval','#nav-over','#floatArea','.sure1','layer',"prov");
        //操作机型
        clickLayer('#czjx-selectval','#nav-over','#floatArea2','.cancel2','layer',"equip");
        clickLayer('#czjx-selectval','#nav-over','#floatArea2','.sure2','layer',"equip");
        //从业年限
        clickLayer('#cynx-selectval','#nav-over','#floatArea5','.cancel5','layer',"year");
        clickLayer('#cynx-selectval','#nav-over','#floatArea5','.sure5','layer',"year");



    }

})(Zepto, window.SY=window.SY || {});
