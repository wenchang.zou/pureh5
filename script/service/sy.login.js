/**
/**
 * Created by cherry on 2015/4/29.
 */
;(function($,M){
    var $mobile=$(".mobile"),
        $verifi=$(".verifi");

    function init(){
        //flushData();
    }
    function initEvent(){

        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){
                case "login-btn" :flushData(); return !1;

            }
        });
        $mobile.on("blur",function(){
            checkNumber($(this).val());
        });
        $verifi.on("blur",function(){
            checkCode($(this).val());
        });


    }
   
    function checkNumber(value){
        var isRight;
        if(!/^(13|15|17|18)(\d{9})/g.test(value)){
            SY.alertBox({
                type:"mini",
                msg:"请输入正确的手机号码"
            });
            isRight=false;
        }else{
            isRight=true;
        }
        return isRight;

    }
    function checkCode(value){
        var isRight;
        if(!/\d{4,8}/g.test(value)){
            SY.alertBox({
                type:"mini",
                msg:"请输入4至8位数字验证码"
            });
            isRight=false;
        }else{
            isRight=true;
        }
        return isRight;
    }
    function flushData(){
      
        if(!checkNumber($mobile.val()) || !checkCode($verifi.val())) return;
        $H.redirect("index.html",{mobile:$mobile.val(),verifi:$verifi.val()})
    }   
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
       
    }

    $(function(){
        preCreate();
        create();
    });
})(Zepto, window.SY=window.SY || {});
