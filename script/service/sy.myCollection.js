/**
 *
 */
;(function($,M){
    var
        $collectMain=$(".collect-con"),
        $colectNew=$(".new-collect-con"),
        TPL_CON=[
            '<ul class="collect-peo">',
                '<li class="coll-header">',
                    '<span><img src="{collectSrc}" alt=""/></span>',
                '</li>',
                '<li class="coll-container">',
                    '<div class="title-name">',
                        '<span class="title-main">{peoName}</span>',
                        '<i>{isRegister}</i>',
                        '<span class="{isOffClass}">[{isOffLine}]</span>',
                    '</div>',

                    '<div class="sub-title">{wellDone}</div>',

                    '<div class="star-con" style="display:{isBlock}">',
                        '<i class="star-yes" style="width:{percentage}%"></i>',
                        '<i class="star-no"></i>',
                    '</div>',
                '</li>',
                '<li class="care-con" >',
                    '<span class="care-txt {grayBg}">{isCare}关注</span>',
                '</li>',
            '</ul>'
        ].join(''),
        EQUIP_CON=[
            '<div class="equip-col-pare" data-eqiup-id="{eqiupId}">',
            '<ul class="equip-col-list">',
            '<li class="header-area"><a href="sbDetail.html"><img src="{productSrc}" alt=""/></a></li>',
            '<li class="list-left">',

            '<a href="sbDetail.html">',
            '<div class="the-tit">',
            '<div class="title-main">{equipName}</div>',

            '<span class="mark-txt {isGray}"><i>{isRegister}</i></span>',
            '</div>',

            '<div class="sub-title">',
            '<span>购买年限:{year}</span>',
            '<span>里程数:{kiloMeter}km</span>',
            '</div>',

            '<div class="circle-con">',
                '<span class="the-cirle"><i>保</i></span>',
                '<span class="the-cirle"><img src="images/nospliter/smallCar.png" alt=""/></span>',
            '</div>',
            '</a>',
            '</li>',

            '</ul>',
            '</div>'
        ].join(''),
        COM_CON=[
            '<li>',
                '<a href="zlgsDetail.html" class="fix">',
                    '<div class="zlgs-left"><img src="{comSrc}" alt=""></div>',
                    '<div class="zlgs-right">',
                         '<h3>{companyName}</h3>',
                         '<p>{mainProduct}</p>',
                        '<p><i class="ico-location"></i>{theAddress}</p>',
                    '</div>',
            '</a>',
        '</li>'].join(''),
        RENT_CON=[
            '<div class="rent-col-pare">',
                '<ul class="rent-col-list">',
                    '<li class="list-left"><a href="qzDetail.html">',
                        '<div class="the-tit">{proName}</div>',

                        '<div class="sub-title">',
                            '<span class="sub-left">{amount}</span>',
                            '<span class="sub-right">{theDate}</span>',
                        '</div>',

                        '<div class="price-con">{priceCon} <span class="price-unit">元/台</span></div></a>',
                    '</li>',



            '</ul>',
            '</div>'
        ].join('');
    function init(){
        flushData(0, $(".list0"), "li", "cur");
    }
    function initEvent(){
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
                switch(eventName){
                    case "equip-btn":flushData(0, $target, "li", "cur");

                        return !1;
                    case "rent-btn":flushData(1, $target, "li", "cur");

                        return !1;
                    case "company-btn":flushData(2, $target, "li", "cur");

                        return !1;
                    case "opera-btn":flushData(3, $target, "li", "cur");

                        return !1;
                    case "engineer-btn":flushData(4, $target, "li", "cur");

                        return !1;


                }
        });

    }

    function flushData(types,dom,cDom,cls){
        dom.addClass(cls).siblings(cDom).removeClass(cls);

        switch(types){
            case 0:flushEquip(); return !1;
            case 1:flushRent(); return !1;
            case 2:flushCompany(); return !1;
            case 3:flushOpera(); return !1;
            case 4:flushEngineer();return !1;


        }
        function flushRent(){
            var len, arr=[],items;
            M.Req.rentReq(function(data){
                len=data.length;
                for(var i=0; i<len; i++){
                    items=data[i];
                    arr.push(RENT_CON.format(items));
                }
                $collectMain.empty();
                $colectNew.empty();
                $collectMain.append(arr.join(''));


            });
        }
        function flushEquip(){
            var len, arr=[],items;
            M.Req.equipReq(function(data){
                len=data.length;
                for(var i=0; i<len; i++){
                    items=data[i];
                    arr.push(EQUIP_CON.format(items));
                }
                $collectMain.empty();
                $colectNew.empty();
                $collectMain.append(arr.join(''));


            });
        }
        function flushCompany(){
            var len, arr=[],items;
            M.Req.compReq(function(data){
                len=data.length;
                for(var i=0; i<len; i++){
                    items=data[i];
                    items.isBlock="none";
                    arr.push(COM_CON.format(items));
                }
                $collectMain.empty();
                $colectNew.empty();
                $colectNew.append(arr.join(''));
            });
        }
        function flushOpera(){
            var len, arr=[],items;
            M.Req.collReq(function(data){
                len=data.length;
                for(var i=0; i<len; i++){
                    items=data[i];
                    items.isBlock="none";
                    arr.push(TPL_CON.format(items));
                }
                $collectMain.empty();
                $colectNew.empty();
                $collectMain.append(arr.join(''));
            });
        }
        function flushEngineer(){
            var len, arr=[],items;
            M.Req.collReq(function(data){
                len=data.length;
                for(var i=0; i<len; i++){
                    items=data[i];
                    items.isBlock="block";
                    arr.push(TPL_CON.format(items));
                }
                $collectMain.empty();
                $colectNew.empty();
                $collectMain.append(arr.join(''));
            });
        }


    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        var len=document.querySelectorAll(".equipment-pare").length,
            rentP=document.querySelectorAll(".equipment-pare");
        for(var i=0; i<len; i++){
            rentP[i].setAttribute('id','J_index_slide'+i);
            SY.SlideTranStatic('#J_index_slide'+i,'10');
        }

    }
    $(function(){

        create();
    });
})(Zepto, window.SY=window.SY || {});
