/**
 * Created by cherry on 2015/4/29.
 */
;(function($,M){
    var $liFst=$(".link-list-first"),
        $liSec=$(".link-list-sec"),
        $info=$(".information-con"),
        $topB=$(".top-bar"),
        session=window.sessionStorage,
        idx0=parseInt(session.ids),
        idx1=parseInt(session.id1),
        idx2=parseInt(session.id2),
        $iconUl= $(".icons-con ul"),
        TOP_INFO=[
            '<ul>',
                '<li data-event-name="my-header-btn">',
                    '<a href="javascript:history.go(-1);" class="back-link"></a>',
                '</li>',
                '<li>我的</li>',
                '<li></li>',

            '</ul>'].join(''),
        INFO_CON=[
            '<dl class="slider-pare" data-event-name="my-btn">',
                '<dt data-event-name="my-btn"><img src="{src}" alt="" data-event-name="my-btn" /></dt>',
                '<dd>',
                    '<div class="title-con"  data-event-name="my-btn">{userName}</div>',
                    '<div class="sub-title"  data-event-name="my-btn">账号：{acount}</div>',
                '</dd>',
            '</dl>'
        ].join(''),
        LIST_CON=[
            '<li class="{className}" data-event-name="{eventBtn}">',
                '<p data-event-name="{eventBtn}">{firstTitle}</p>',
                '<p data-event-name="{eventBtn}">{secondTitle}</p>',
            '</li>'
        ].join('');

    function init(){
        flushData();
    }
    function initEvent(){
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){
                case "my-btn": $H.redirect("myInformation.html");return !1;
                case "equipment-btn": $H.redirect("myEquiment.html");return !1;
                case "rent-btn": $H.redirect("myRent.html");return !1;
                case "collection-btn": $H.redirect("myCollection.html");return !1;
                case "message-btn": $H.redirect("myMessage.html");return !1;
                case "setting-btn": $H.redirect("mySetting.html");return !1;
        }
        });
        if($H.getURLParam().onName=="on"){
            $iconUl.children("li").eq(idx0).addClass("on");
            $iconUl.children("li").eq(idx1).addClass("on");
            $iconUl.children("li").eq(idx2).addClass("on");
        }
        $(".icons-con ul").children("li").each(function(i){
            var $id,$this=$(this);
            $(this).on(M.EVENT.CLICK,function(){
                $id=$(this).attr("data-idinify");


                switch($id){
                    case "0" : if($H.getURLParam().onName=="on" &&  session.ids==idx0){

                                    $H.redirect("czsDetail.html",{idinify:$id});
                                }else{
                                    $H.redirect("myCzsReg.html",{idinify:$id});
                                }
                        return !1;

                    case "1" :
                            if($H.getURLParam().onName=="on" && session.id1==idx1 ){

                                $H.redirect("gcsDetail.html",{idinify:$id});
                            }else{
                                $H.redirect("myGcsReg.html",{idinify:$id});
                            }
                        return !1;

                    case "2" :
                        if($H.getURLParam().onName=="on" && session.id2==idx2 ){
                            $H.redirect("zlgsDetail.html",{idinify:$id});
                        }else{
                            $H.redirect("myZlgsReg.html",{idinify:$id});
                        }
                        return !1;

                }
            });
        });
    }
    function flushData(){
        var len, arr=[],arr1=[],items,items1,arr2=[],arr3=[];
        M.Req.listCon(function(data){
            len=data.length;
            for(var i=0; i<2; i++){
                items=data[i];
                arr.push(LIST_CON.format(items));
            }
            for(var j=2; j<len; j++){
                items1=data[j];
                arr1.push(LIST_CON.format(items1));
            }
            $liFst.append(arr.join(''));
            $liSec.append(arr1.join(''));

        });
        M.Req.infoReq(function(data){
            arr2.push(TOP_INFO.format(data));
            arr3.push(INFO_CON.format(data));
            $info.append(arr3.join(''));
            $topB.append(arr2.join(''));
        })
    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){

    }
    $(function(){
        preCreate();
        create();
    });
})(Zepto, window.SY=window.SY || {});
