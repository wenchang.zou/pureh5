/**
 *
 */
;(function($,M){
    var
        $equipMain=$(".equipment-con"),
        TPL_CON=[
            '<div class="equipment-pare">',
                '<ul class="equipment-list">',
                    '<li class="header-area" data-eqiup-id="{eqiupId}"><img src="{productSrc}" alt=""/></li>',
                    '<li class="list-left" data-eqiup-id="{eqiupId}">',

                        '<div class="the-tit">',
                            '<div class="title-main">{equipName}</div>',

                            '<span class="mark-txt {isGray}"><i>{isRegister}</i></span>',
                        '</div>',

                        '<div class="sub-title">',
                            '<span class="sub-left">{amount}</span>',
                            '<span class="sub-right">{theDate}</span>',
                        '</div>',

                        '<div class="pull-con {isPull}">已{isPullOn}架</div>',
                    '</li>',
                    '<li class="hide-area">',
                        '<div class="update-btn" data-event-name="update-btn" data-eqiup-id="{eqiupId}">更新设备</div>',
                        '<div class="down-btn" data-event-name="off-btn">下架</div>',
                    '</li>',
                '</ul>',
        '</div>'
        ].join('');
    function init(){
        flushData();
    }
    function initEvent(){

        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){


            }
        });
    }
    function toUpdate(dom,url){
        var $eqiupId;
        $(dom).each(function(){
            $(document).on(M.EVENT.CLICK, dom, function(e){
                $eqiupId=$(this).attr("data-eqiup-id");
                $H.redirect(url,{eqiupId:$eqiupId});
            });
        });
    }
    function downOff(dom,dDom, cDom,ulDom){
        $(dom).each(function(i){
            var $this=$(this);
            $(this).find(dDom).on(M.EVENT.CLICK, function(){
                $this.find(cDom).html("已下架");
                $this.find(cDom).addClass("gray-pull");
                $this.children(ulDom).css('left',0);
            });


        })
    }

    function flushData(){
        var len, arr=[],items;
        M.Req.equipReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                items=data[i];
                arr.push(TPL_CON.format(items));
            }
            $equipMain.append(arr.join(''));
            preCreate();
            downOff(".equipment-pare",".down-btn",".pull-con","ul");
            toUpdate(".header-area","mySbDetail.html");
            toUpdate(".list-left","mySbDetail.html");
            toUpdate(".update-btn","mySbUpate.html");

        });

    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        var len=document.querySelectorAll(".equipment-pare").length,
            rentP=document.querySelectorAll(".equipment-pare");
        for(var i=0; i<len; i++){
            rentP[i].setAttribute('id','J_index_slide'+i);
            SY.SlideTranStatic('#J_index_slide'+i,'10','.equipment-pare');

        }


    }

    $(function(){

        create();
    });
})(Zepto, window.SY=window.SY || {});
