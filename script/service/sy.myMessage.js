/**
 * Created by cherry on 2015/4/29.
 */
;(function($,M){
    var
        $rentMain=$(".message-con"),
        TPL_CON=[
            '<div class="message-pare" data-message="{messageId}">',
                '<ul class="msg-list">',
                    '<li>',
                        '<div class="header-left"><img src="{headSrc}" alt=""/>',
                        '<span class="{redPointer}"></span></div>',
                        '<div class="mess-con">',
                            '<p class="mess-title">{mainTitle}</p>',

                            '<p class="mess-sub">{subMessage}</p>',
                        '</div>',
                        '<div class="times-con">{timesCon}</div>',
                    '</li>',
                    '<li class="right-area">',
                        '<div class="ignore">忽略</div>',
                        '<div class="response">回复</div>',
                    '</li>',
                '</ul>',
            '</div>'
        ].join('');
    function init(){
        flushData();
    }
    function initEvent(){
        $(document).on(M.EVENT.CLICK,function(e){
            var target= e.target,
                $target=$(target),
                dataset=target.dataset,
                eventName=dataset["eventName"];
            switch(eventName){


            }
        });
    }

    function flushData(){
        var len, arr=[],items;
        M.Req.msgReq(function(data){
            len=data.length;
            for(var i=0; i<len; i++){
                items=data[i];
                arr.push(TPL_CON.format(items));
            }
            $rentMain.append(arr.join(''));
            preCreate();

        });

    }
    function create(){
        init();
        initEvent();
    }
    function preCreate(){
        var len=document.querySelectorAll(".message-pare").length,
            rentP=document.querySelectorAll(".message-pare");
        for(var i=0; i<len; i++){
            rentP[i].setAttribute('id','J_index_slide'+i);
            SY.SlideTranStatic('#J_index_slide'+i,'7.15','.message-pare');
        }
    }
    $(function(){

        create();
    });
})(Zepto, window.SY=window.SY || {});
