/**
 * Created by cherry on 2015/4/23.
 */
;(function($, M){

    M.Req={

        getDefError: function(data){
            $H.tipNetError();
        },
        getWholeURL: function(url){
            return url.indexOf('http') === -1 ? (M.Const.PRO_PATH + url) : url;
        },



        /**
         * 底层请求
         */
        req:function(url,success,data,async,error){
            //默认错误设置
            !error && (error = this.getDefError);

            $.ajax({
                url: this.getWholeURL(url),
                success:  success,
                error: error,
                async: async === undefined ? true : async,
                dataType: 'json',
                data:JSON.stringify(data),
                type: 'post',
                timeout: 60000,
                xhrFields:{
                    'withCredentials': 'true'   //凭证
                }
            });

        },
      reqImg:function(url,success,data,async,error){
        //默认错误设置
        !error && (error = this.getDefError);

        $.ajax({
          url: this.getWholeURL(url),
          success:  success,
          error: error,
          async: async === undefined ? true : async,
          dataType: 'json',
          data:data,
          type: 'post',
          timeout: 60000,
          processData:false,
          contentType:false,
            xhrFields:{
            'withCredentials': 'true'   //凭证
          }
        });

      },
        listCon:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.HOME_DATA);
        },
        infoReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.HEAD_DATA);
        },
        rentReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.RENT_DATA);
        },
        msgReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.MSG_DATA);
        },
        equipReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.EQUIP_DATA);
        },
        collReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.COLL_DATA);
        },
        compReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.COM_DATA);
        },
        cityReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.PROVINCE);
        },
        eqClassReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.EQUIP);
        },
        yearReq:function(successFn, data, errorFn, error){
            //this.req(TestData.SALE_TITLE,successFn,data);
            successFn(TestData.YEARS);
        }
    }
})(Zepto,window.SY=window.SY|| {});
