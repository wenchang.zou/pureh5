/**
 * Created by Administrator on 2015/4/23.
 */
window.TestData={
    HEAD_DATA:{
        src:"images/nospliter/header.jpg",
        userName:"Amn.",
        acount:"13213924137"
    },

    HOME_DATA:[
        {
            className:"list-one",
            eventBtn:"equipment-btn",
            firstTitle:"设备",
            secondTitle:"赶紧去录设备"
        },
        {
            className:"list-tow",
            eventBtn:"rent-btn",
            firstTitle:"求租",
            secondTitle:""
        },
        {
            className:"list-thr",
            eventBtn:"collection-btn",
            firstTitle:"收藏",
            secondTitle:""
        },
        {
            className:"list-four",
            eventBtn:"message-btn",
            firstTitle:"消息",
            secondTitle:""
        },

        {
            className:"list-five",
            eventBtn:"setting-btn",
            firstTitle:"设置",
            secondTitle:""
        }
   ],
    RENT_DATA:[
    {
        proName:"湖南常德求租3台9018车载泵",
        isGray:"",
        isRegister:"已认证",
        amount:"8000",
        theDate:"2015/9/4",
        priceCon:"4000",
        rentId:"0"


    },
    {
        proName:"湖南常德求租3台9018车载泵11",
        isGray:"",
        isRegister:"已认证",
        amount:"8000",
        theDate:"2015/9/4",
        priceCon:"4000",
        rentId:"1"
    },
    {
        proName:"湖南常德求租3台9018车载泵11",
        isGray:"gray-bg",
        isRegister:"未认证",
        amount:"8000",
        theDate:"2015/9/4",
        priceCon:"面议",
        rentId:"2"
    }
    ],
    MSG_DATA:[
        {
            headSrc:"images/nospliter/default.png",
            mainTitle:"纳木错",
            redPointer:"mess-pointer",
            subMessage:"尊敬的用户您好，平台将于明日07：00第一个....",
            timesCon:"2015/09/04",
            messageId:"0"

        },
        {
            headSrc:"images/nospliter/header.jpg",
            mainTitle:"用户13582316873",
            redPointer:"mess-pointer",
            subMessage:"好的，那下次有机会我们一起干，也有个照应",
            timesCon:"2015/09/04",
            messageId:"1"
        },
        {
            headSrc:"images/nospliter/header01.jpg",
            mainTitle:"用户13582316873",
            redPointer:"",
            subMessage:"好的，那下次有机会我们一起干，也有个照应",
            timesCon:"2015/09/04",
            messageId:"2"
        },
        {
            headSrc:"images/nospliter/header.jpg",
            mainTitle:"用户13582316873",
            redPointer:"mess-pointer",
            subMessage:"好的，那下次有机会我们一起干，也有个照应",
            timesCon:"2015/09/04",
            messageId:"3"
        },
        {
            headSrc:"images/nospliter/header01.jpg",
            mainTitle:"用户13582316873",
            redPointer:"",
            subMessage:"好的，那下次有机会我们一起干，也有个照应",
            timesCon:"2015/09/04",
            messageId:"4"
        }
    ],
    EQUIP_DATA:[
        {
            productSrc:"images/nospliter/pro1.jpg",
            equipName:"湖南常德求租3台9018车载泵",
            isGray:"",
            isRegister:"已认证",
            amount:"8000",
            theDate:"2015/9/4",
            isPull:"",
            isPullOn:"上",
            year:"3",
            kiloMeter:"10000",
            eqiupId:"0"

        },
        {
            productSrc:"images/nospliter/pro1.jpg",
            equipName:"湖南常德求租3台9018车载泵11",
            isGray:"",
            isRegister:"已认证",
            amount:"8000",
            theDate:"2015/9/4",
            isPull:"",
            isPullOn:"上",
            year:"3.5",
            kiloMeter:"12000",
            eqiupId:"1"
        },
        {
            productSrc:"images/nospliter/pro1.jpg",
            equipName:"湖南常德求租3台9018车载泵11",
            isGray:"gray-bg",
            isRegister:"未认证",
            amount:"8000",
            theDate:"2015/9/4",
            isPull:"gray-pull",
            isPullOn:"下",
            year:"4",
            kiloMeter:"15000",
            eqiupId:"2"

        }
    ],
    COLL_DATA:[
        {
            collectSrc:"images/nospliter/header.jpg",
            peoName:"王师傅",
            isRegister:"已认证",
            isOffClass:"color-yes",
            isOffLine:"在线",
            wellDone:"擅长设备：汽车吊、履带吊",
            grayBg:"",
            isCare:"已",
            percentage:"80"

        },
        {
            collectSrc:"images/nospliter/header01.jpg",
            peoName:"王师傅",
            isRegister:"已认证",
            isOffClass:"color-yes",
            isOffLine:"在线",
            wellDone:"擅长设备：汽车吊、履带吊",
            grayBg:"gray-bg",
            isCare:"未",
            percentage:"50"
        },
        {
            collectSrc:"images/nospliter/header.jpg",
            peoName:"王师傅",
            isRegister:"已认证",
            isOffClass:"color-yes",
            isOffLine:"在线",
            wellDone:"擅长设备：汽车吊、履带吊",
            grayBg:"gray-bg",
            isCare:"未",
            percentage:"90"
        },
        {
            collectSrc:"images/nospliter/header01.jpg",
            peoName:"王师傅",
            isRegister:"已认证",
            isOffClass:"color-no",
            isOffLine:"离线",
            grayBg:"",
            wellDone:"擅长设备：汽车吊、履带吊",
            isCare:"已",
            percentage:"70"

        }
    ],
    COM_DATA:[
        {
            comSrc:"images/nospliter/pro02.jpg",
            companyName:"合肥庆华建筑机械租赁有限公司",
            mainProduct:"主营：塔式起重机，施工升降机租赁等...",
            theAddress:"合肥市包河大道与纬四路交叉口"
        },
        {
            comSrc:"images/nospliter/pro02.jpg",
            companyName:"江苏庆华建筑租赁有限公司",
            mainProduct:"主营：塔式起重机，施工升降机租赁等...",
            theAddress:"合肥市包河大道与纬四路交叉口"
        },
        {
            comSrc:"images/nospliter/pro02.jpg",
            companyName:"湖南庆华建筑租赁有限公司",
            mainProduct:"主营：塔式起重机，施工升降机租赁等...",
            theAddress:"合肥市包河大道与纬四路交叉口2"
        },
        {
            comSrc:"images/nospliter/pro02.jpg",
            companyName:"合肥庆华建筑机械租赁有限公司",
            mainProduct:"主营：塔式起重机，施工升降机租赁等...",
            theAddress:"合肥市包河大道与纬四路交叉口1"
        }
    ],
    PROVINCE:[
        {
            "name": "上海",
            "parent": "0",
            "id" : 1
        },
        {
            "name": "上海市",
            "parent": 1,
            "id" : 2
        },
        {
            "name": "长宁区",
            "parent": 2,
            "id" : 3
        },
        {
            "name": "徐汇区",
            "parent": 2,
            "id" : 8
        },
        {
            "name": "北京",
            "parent": "0",
            "id" :4
        }
        ,{
            "name": "北京市",
            "parent": 4,
            "id" :9
        }
        ,{
            "name": "丰台区",
            "parent": 9,
            "id" :10
        },{
            "name": "海淀区",
            "parent": 9,
            "id" :11
        }
        ,{
            "name": "湖南省",
            "parent": "0",
            "id" :5
        }
        ,{
            "name": "长沙市",
            "parent": 5,
            "id" :20
        },{
            "name": "芙蓉区",
            "parent":20,
            "id" :100
        }
        ,{
            "name": "天心区",
            "parent":20,
            "id" :101
        }
        ,{
            "name": "娄底市",
            "parent": 5,
            "id" :21
        },{
            "name": "双峰县",
            "parent":21,
            "id" :120
        }
        ,{
            "name": "涟源市",
            "parent":21,
            "id" :121
        }
    ],
    EQUIP:[
        {
            "name": "土方机械",
            "parent": "0",
            "id" : 1
        },
        {
            "name": "高空作业平台",
            "parent": 1,
            "id" : 2
        },
        {
            "name": "桥式起重机",
            "parent": 1,
            "id" : 3
        },
        {
            "name": "门式起重机",
            "parent": 1,
            "id" : 8
        },
        {
            "name": "路面机械",
            "parent": "0",
            "id" :4
        }
        ,{
            "name": "叉车",
            "parent": 4,
            "id" :9
        }
        ,{
            "name": "随车吊",
            "parent":4,
            "id" :10
        },{
            "name": "塔吊",
            "parent":41,
            "id" :11
        }
        ,{
            "name": "起重机械",
            "parent": "0",
            "id" :5
        }
        ,{
            "name": "桥式起重机",
            "parent": 5,
            "id" :20
        }
        ,{
            "name": "门式起重机",
            "parent":5,
            "id" :101
        }
        ,{
            "name": "升降机",
            "parent": 5,
            "id" :21
        },{
            "name": "工作平台",
            "parent":5,
            "id" :120
        }

    ],
    YEARS:[
        {
            yearId:"0",
            years:"1年"
        },
        {
            yearId:"1",
            years:"2年"
        },
        {
            yearId:"2",
            years:"3年"
        },
        {
            yearId:"3",
            years:"4年"
        },
        {
            yearId:"4",
            years:"5年"
        },
        {
            yearId:"5",
            years:"6年"
        },
        {
            yearId:"6",
            years:"7年"
        },
        {
            yearId:"7",
            years:"8年"
        },
        {
            yearId:"8",
            years:"9年"
        },
        {
            yearId:"9",
            years:"10年"
        }
    ]


}
