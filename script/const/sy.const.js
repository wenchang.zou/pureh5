﻿/**
 * url
 * @author *
 * @action 常量配置类
 * @desc   界面所有涉及的路径，URL等可配置常量都从这里读取。
 * @warn   修改此文件后，请务必验证一把！！！
 */
(function(M){
	M.Const = {
		// PRO_PATH:'http://yun1234.weipos.com/SingleWeipos/',
		PRO_PATH:'http://127.0.0.1:8080/weipos/'
		
	}
})(window.SY = window.SY|| {});
